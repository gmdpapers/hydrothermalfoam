.. include:: /include.rst_

.. _installation_source:

Build from source
==========================

Install OpenFOAM
------------------------

The |foam| v1.0 is developed based on OpenFOAM-7, which can be installed according to the installation instructions (https://openfoam.org/download/) given by the development team for 
`Ubuntu Linux <https://openfoam.org/download/7-ubuntu/>`_, 
`Other Linux <https://openfoam.org/download/7-linux/>`_, 
`macOS <https://openfoam.org/download/7-macos/>`_ and `Windows <https://openfoam.org/download/windows-10/>`_ platform, respectively.

.. _installation_thermophysical:

Build HydrothermalFoam
----------------------------

Once OpenFOAM is built successfully, 
the source code of |foam| be downloaded 
from `Zenodo <https://doi.org/10.5281/zenodo.3755648>`_ or 
from `GitLab repository <https://gitlab.com/gmdpapers/hydrothermalfoam>`_. 
The directory structure and components ofHydrothermalFoamare shown in :numref:`fig_file_structure_main` and the components canbe built follow three steps below,

.. figure:: /_figures/filetree_main.*
    :width: 500 px
    :align: center
    :name: fig_file_structure_main

    Structure and components of the |foam| toolbox.

.. note::

    The following steps are only proper for Mac OS and Linux systems, 
    we do not yet build HydrothermalFoam on Windows system directly.
    If users using ubuntu sub-system on Windows 10, 
    the following steps could work in the sub-system.

.. _installation_thermophysical_build_freesteam:

Build freesteam-2.1 library
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. tab:: New style

    **After 16 Dec. 2020** [502c669e_ ], we decide to give up scons_ and use cmake_ reorganize the freesteam library project, because cmake_ is much more straightforward than scons_.
    Now user just need to run shell script of :code:`makeFreeSteam` in the :code:`libraries` folder,
    then the freesteam library will be compiled and copied to :code:`$FOAM_USER_LIBBIN` path.

.. tab:: Old style

    The original freesteam project is constructed by scons_, which is a open source software constructiontool dependent on `python 2`_, and based on GSL_ (GNUScientificLibrary). Therefore python 2, scons and GSL have to be installed firstly, then change directory to freesteam-2.1 in HydrothermalFoam source code and type command of :code:`scons INSTALL_PREFIX=$FOAM_USER_LIBBIN install` to compile freesteam library named :code:`libfreesteam.so`. See home page of freesteam-2.1_ project for more details.

Build libraries of customized boundary conditions and thermo-physical model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Change directory to libraries and type command of :code:`./Allmake` to compile the libraries named :code:`libHydroThermoPhysicalModels.so` and :code:`libHydrothermalBoundaryConditions.so`. 

.. warning::

    It should be noted that if you are using OpenFOAM-8, you have to run `./Allmake-8` to compile thermo-physical model to compatible with OpenFOAM-8. And of course the `Allmake` script is designed for OpenFOAM-7. **This is the only difference between version 7 and version 8.**

Build solver of HydrothermalSinglePhaseDarcyFoam
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Change directory to HydrothermalSinglePhaseDarcyFoam and type command of :code:`wmake` to compile the solver named :code:`HydrothermalSinglePhaseDarcyFoam`.

.. note::

    All the library files and executable application (solver) file will be generated in directories defined by OpenFOAM’s path variables of :code:`FOAM_USER_LIBBIN` and :code:`FOAM_USER_APPBIN`, respectively. 
    If build HydrothermalFoam in Mac OS, the extension of the library files is :code:`.dylib`, 
    please make a symbolic links. 
    For example, see following command for :code:`libHydroThermoPhysicalModels.dylib` 

    .. code-block:: bash

        ln -s $FOAM_USER_LIBBIN/libHydroThermoPhysicalModels.dylib $FOAM_USER_LIBBIN/libHydroThermoPhysicalModels.so