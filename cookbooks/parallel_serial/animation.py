#!/Users/zguo/.pyenv/shims/python
# -*-coding:utf-8-*-
# Plot OpenFOAM 2D model results which generated from `postProcess -func surfaces`
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# 'Zhikui Guo, 2020/06/28, GEOMAR
# ===============================================================
from matplotlib.animation import FuncAnimation
import matplotlib as mpl
import matplotlib.animation as animation
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np
import os
from console_progressbar import ProgressBar
from colored import fg, bg, attr
C_GREEN=fg('green')
C_RED=fg('red')
C_BLUE=fg('blue')
C_DEFAULT=attr('reset')
import sciPyFoam.postProcessing.cuttingPlane as pc
import sciPyFoam.figure as scifig
import getopt, sys
from colored import fg, bg, attr
C_GREEN = fg('green')
C_RED = fg('red')
C_BLUE = fg('blue')
C_DEFAULT = attr('reset')
#===============================================================
# config font
mpl.rcParams['font.family'] = 'Arial'
mpl.rcParams['mathtext.fontset'] = 'cm'
scifig.usePaperStyle(mpl,fontsize=12)
# ===========================================================
def usage(argv):
    basename = argv[0].split('/')
    basename = basename[len(basename)-1]
    description='Plot OpenFOAM 2D model results which generated from `postProcess -func surfaces`'
    num_symbol=int((len(description)+20 - len(basename))/2)
    head='='*num_symbol+basename+'='*num_symbol
    print(head)
    print(description)
    print('Zhikui Guo, 2020/06/28, GEOMAR')
    print('[Example]: '+C_RED + basename+C_BLUE + ' caseDir [genAnimation=True] [saveAnimation=True] [fieldName] [model]'+C_DEFAULT)
    print('='*len(head))
def getTimes(postProcessDataPath):
    times=os.listdir(postProcessDataPath)
    timeDirs=[]
    for t in times:
        if(os.path.isdir(postProcessDataPath+t)):
            timeDirs.append(t)
        else:
            print(t,'is not a directory')
    times=np.array(timeDirs,dtype=int)
    times=np.sort(times)
    return times
def plot_animation(caseDir, indTime_PDF=-1, levels=60,coord2km=True, fieldName='T',label_cb='Temperature ($^{\circ}$C)',
    model='HydrothermalFoam model',cmap='Spectral_r',genAnimation=False,saveAnimation=False,dpi_out=400,figwidth=20,w_offset_cm=0,path_out='.',color_label='w'):
    postProcessDataPath=caseDir+'/postProcessing/surfaces/'
    # 1. get times
    times=getTimes(postProcessDataPath)
    # 2. read the latest time
    datapath=postProcessDataPath+str(times[-1])
    filename=datapath+'/'
    name_fmt=lambda  name : name + '_zNormal.vtk'
    # read data
    triangles,T=pc.Read_VTK_POLYDATA(datapath,fieldName,name_fmt=name_fmt,coord2km=coord2km,depthPositive=True)
    # plot
    ax_field, ax_cb, CSf, cb,vmin,vmax=pc.plotField(None,triangles, T,figwidth=figwidth, w_offset_cm=w_offset_cm)
    fig=plt.gcf()
    # ax_field.xaxis.set_major_locator(MultipleLocator(200))
    # ax_field.xaxis.set_minor_locator(MultipleLocator(20))
    # ax_field.yaxis.set_major_locator(MultipleLocator(100))
    # ax_field.yaxis.set_minor_locator(MultipleLocator(20))
    ax_field.set_xlabel('x (km)' if (coord2km) else 'x (m)')
    ax_field.set_ylabel('Depth (km)' if(coord2km) else 'Depth (m)')
    cb.set_ticks(MultipleLocator(100))
    cb.set_label(label_cb)
    plt.tight_layout(pad=0)
    # init plot
    for coll in CSf.collections: 
            ax_field.collections.remove(coll) 
    datapath=postProcessDataPath+str(times[indTime_PDF])
    filename=datapath+'/'
    triangles,T=pc.Read_VTK_POLYDATA(datapath,fieldName,name_fmt=name_fmt,coord2km=coord2km,depthPositive=True)
    # p=[ax_field.tricontourf(triangles,T,levels=levels,cmap=cmap,vmin=vmin,vmax=vmax)]
    p=[ax_field.tripcolor(triangles,T,shading='gouraud',cmap=cmap,vmin=vmin,vmax=vmax)]
    text=[ax_field.text(0.02,0.98,str('%.1f years    %s' % (times[indTime_PDF]/86400/365, model)),color=color_label,fontsize=14,fontweight='bold',ha='left',va='top',transform=ax_field.transAxes)]
    # progressbar
    pb = ProgressBar(total=len(times),prefix=C_BLUE+'Progress: '+C_DEFAULT, suffix=' Completed'+C_DEFAULT, decimals=3, length=50, fill=C_GREEN+'#', zfill=C_DEFAULT+'-')

    def update(i):
        time=times[i]
        p[0].remove()
        text[0].remove()
        datapath=postProcessDataPath+str(time)
        filename=datapath+'/'
        triangles,T=pc.Read_VTK_POLYDATA(datapath,fieldName,name_fmt=name_fmt,coord2km=coord2km,depthPositive=True)
        # p[0]=ax_field.tricontourf(triangles,T,levels=levels,cmap=cmap,vmin=vmin,vmax=vmax)
        p[0]=ax_field.tripcolor(triangles,T,shading='gouraud',cmap=cmap,vmin=vmin,vmax=vmax)
        text[0]=ax_field.text(0.02,0.98,str('%.1f years    %s' % (time/86400/365, model)),color=color_label,fontsize=14,fontweight='bold',ha='left',va='top',transform=ax_field.transAxes)
        if(time==times[-1]):
            plt.savefig(path_out+'/'+fieldName+'_'+model+'.pdf')
        pb.print_progress_bar(i+1)

    # 根据不同的movie格式设置相应的writter
    def animationWriter(fmt='mp4'):
        if(fmt_movie=='mp4'):
            Writer = animation.writers['ffmpeg']
            writer = Writer(fps=15, metadata=dict(artist='Zhikui Guo, et al., 2020, GMD',title='Cookbook of HydrothermalFoam tools',copyright='Zhikui Guo, 2018',comment='HydrothermalFoam open source tools for hydrothermal modeling'), bitrate=1800)
        elif(fmt_movie=='avi'):
            Writer = animation.writers['avconv']
            writer = Writer(fps=15, metadata=dict(artist='Zhikui Guo, et al., 2020, GMD',title='Cookbook of HydrothermalFoam tools',copyright='Zhikui Guo, 2018',comment='HydrothermalFoam open source tools for hydrothermal modeling'), bitrate=1800)
        elif(fmt_movie=='gif'):
            writer='imagemagick'
        else:
            print('暂不支持此movie格式(mp4,gif): ',fmt_movie)
            exit(0)
        return writer
    # animation
    # 动画参数
    interval = 1 #in seconds  
    dpi_out=dpi_out
    repeat=False
    fmt_movie='mp4'
    fname_movie=path_out+'/'+fieldName+'_'+model
    writer=animationWriter(fmt_movie)
    if(genAnimation):
        ani = FuncAnimation(fig, update, len(times),blit=False, interval=interval*1e3,repeat=repeat)
    # 保存为gif或者显示
    if((saveAnimation==True) and (genAnimation==True)):
        ani.save(fname_movie+'.'+fmt_movie, dpi=dpi_out, writer=writer)
    else:
        plt.savefig(path_out+'/'+fieldName+'_'+model+'.pdf')
        plt.show()

import argparse

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--animation", help="generation animation",action="store_true")
    parser.add_argument("-s", "--save", help="save animation",action="store_true")
    parser.add_argument("-t", "--time",type=int, help="time index",default=-1)
    parser.add_argument("-d", "--dpi",type=int, help="animation dpi",default=400)
    parser.add_argument("-c", "--cmap",type=str, help="color map",default='Spectral_r')
    parser.add_argument("--model",type=str, help="model name")
    parser.add_argument("--textColor",type=str, help="text color of label, e.g. time",default='w')
    parser.add_argument("--cbLabel",type=str, help="label of colorbar",default='Temperature ($^{\circ}$C)')
    parser.add_argument("--fieldName",type=str, help="field name",default="T")
    parser.add_argument("--coord2km",help="is convert m to km",action="store_true")
    parser.add_argument("--levels",type=int, help="levels of contour",default=60)
    parser.add_argument("caseDir",type=str, help="case directory")
    parser.add_argument("-w","--figwidth",type=int, help="figure width",default=20)
    parser.add_argument("--w_offset_cm",type=float, help="offset of figure width to adjust margin (unit: cm)",default=0)
    parser.add_argument("-o","--output",type=str, help="output path",default=".")

    args = parser.parse_args()
    # plot
    caseDir=args.caseDir
    absPath_case = os.path.abspath(caseDir)
    model=absPath_case.split('/')[-1]
    if(not (args.model == None)):
        model=args.model
    plot_animation(args.caseDir,args.time,args.levels,args.coord2km,args.fieldName,args.cbLabel,model,
    args.cmap,args.animation,args.save,args.dpi,args.figwidth,args.w_offset_cm,args.output,args.textColor)

if __name__ == '__main__':
    sys.exit(main(sys.argv))

