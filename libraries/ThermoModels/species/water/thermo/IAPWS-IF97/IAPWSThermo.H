/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2011-2019 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::IAPWSThermo

Description
    Constant properties thermodynamics package
    templated into the EquationOfState.

SourceFiles
    IAPWSThermoI.H
    IAPWSThermo.C

\*---------------------------------------------------------------------------*/

#ifndef IAPWSThermo_H
#define IAPWSThermo_H
#include "IAPWS-IF97.H"
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// Forward declaration of friend functions and operators

template<class EquationOfState> class IAPWSThermo;

template<class EquationOfState>
inline IAPWSThermo<EquationOfState> operator+
(
    const IAPWSThermo<EquationOfState>&,
    const IAPWSThermo<EquationOfState>&
);

template<class EquationOfState>
inline IAPWSThermo<EquationOfState> operator*
(
    const scalar,
    const IAPWSThermo<EquationOfState>&
);

template<class EquationOfState>
inline IAPWSThermo<EquationOfState> operator==
(
    const IAPWSThermo<EquationOfState>&,
    const IAPWSThermo<EquationOfState>&
);

template<class EquationOfState>
Ostream& operator<<
(
    Ostream&,
    const IAPWSThermo<EquationOfState>&
);


/*---------------------------------------------------------------------------*\
                           Class IAPWSThermo Declaration
\*---------------------------------------------------------------------------*/

template<class EquationOfState>
class IAPWSThermo
:
    public EquationOfState
{
    // Private Data

    // Private Member Functions

        //- Construct from components
        inline IAPWSThermo
        (
            const EquationOfState& st
        );


public:

        typedef SteamState eosType;
    // Constructors

        //- Construct from dictionary
        IAPWSThermo(const dictionary& dict);

        //- Construct as named copy
        inline IAPWSThermo(const word&, const IAPWSThermo&);

        //- Construct and return a clone
        inline autoPtr<IAPWSThermo> clone() const;

        //- Selector from dictionary
        inline static autoPtr<IAPWSThermo> New(const dictionary& dict);


    // Member Functions

        //- Return the instantiated type name
        static word typeName()
        {
            return "IAPWS<" + EquationOfState::typeName() + '>';
        }

        //- Limit the temperature to be in the range Tlow_ to Thigh_
        inline scalar limit(const scalar T) const;


        // Fundamental properties

            //- Heat capacity at constant pressure [J/kg/K]
            inline scalar Cp(const scalar p, const scalar T) const;

            //- Absolute Enthalpy [J/kg]
            inline scalar Ha(const scalar p, const scalar T) const;

            //- Sensible enthalpy [J/kg]
            inline scalar Hs(const scalar p, const scalar T) const;

            //- Chemical enthalpy [J/kg]
            inline scalar Hc() const;

            //- Entropy [J/kg/K]
            inline scalar S(const scalar p, const scalar T) const;

            #include "HtoEthermo.H"


        // Derivative term used for Jacobian

            //- Derivative of Gibbs free energy w.r.t. temperature
            inline scalar dGdT(const scalar p, const scalar T) const;

            //- Temperature derivative of heat capacity at constant pressure
            inline scalar dCpdT(const scalar p, const scalar T) const;


        // I-O

            //- Write to Ostream
            void write(Ostream& os) const;


    // Member Operators

        inline void operator+=(const IAPWSThermo&);


    // Friend operators

        friend IAPWSThermo operator+ <EquationOfState>
        (
            const IAPWSThermo&,
            const IAPWSThermo&
        );

        friend IAPWSThermo operator* <EquationOfState>
        (
            const scalar,
            const IAPWSThermo&
        );

        friend IAPWSThermo operator== <EquationOfState>
        (
            const IAPWSThermo&,
            const IAPWSThermo&
        );


    // IOstream Operators

        friend Ostream& operator<< <EquationOfState>
        (
            Ostream&,
            const IAPWSThermo&
        );
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "IAPWSThermoI.H"

#ifdef NoRepository
    #include "IAPWSThermo.C"
#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
