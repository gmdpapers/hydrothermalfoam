# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import sys, os
sys.path.append(os.path.abspath('_extensions'))

# -- Project information -----------------------------------------------------

project = 'HydrothermalFoam'
copyright = '2020, Zhikui Guo'
author = 'Zhikui Guo'

# The short X.Y version
version = ''
# The full version, including alpha/beta/rc tags
release = ''


# -- General configuration ---------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.mathjax', 
              'jinja',
              'sphinxcontrib.bibtex',
              'sphinx.ext.graphviz',
              'sphinx.ext.ifconfig',
              'sphinx_inline_tabs',
              'sphinx_sitemap'
              ]
html_baseurl="https://www.hydrothermalfoam.info"  # for sitemap
sphinx_tabs_nowarn=True
graphviz_output_format='svg'
source_encoding = 'utf-8-sig'
source_suffix = '.rst'
master_doc = 'index'
templates_path = ['_templates']
bibtex_bibfiles = ['manual.bib']

# internationalization
language = 'en'
locale_dirs = ['locale/']
gettext_compact = True
gettext_auto_build=True
# Set smartquotes_action to 'qe' to disable Smart Quotes transform of -- and ---
smartquotes_action = 'qe'
# customize OpenFOAM syntax highlight
from sphinx.highlighting import lexers
from pygments_OpenFOAM.foam import OpenFOAMLexer
lexers['foam'] = OpenFOAMLexer(startinline=True)
# default language to highlight source code
highlight_language = 'foam'
pygments_style = 'emacs' # xcode,monokai,emacs,autumn,vs,solarized-dark

# -- Project configuration ------------------------------------------------
project = 'HydrothermalFoam'
copyright = "Zhikui Guo, Lars Ruepke"
# The version shown at the top of the sidebar
version = '1.0'
# The full version shown in the page title
release = '1.0'
# Make the "Edit on GitHub" button link to the correct branch
# Default to master branch if Azure Pipelines environmental variable BUILD_SOURCEBRANCHNAME is not defined
# github_version = os.getenv("BUILD_SOURCEBRANCHNAME", 'master')


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'rtd'
html_theme_path = ["_themes"]
html_theme_options = {
    'sticky_navigation': False,
    'includehidden': False,
    'logo_only' : True,
    'sticky_navigation': True,
    'titles_only': True,
    'display_version': False,
    'prev_next_buttons_location': 'both',
    'style_nav_header_background': 'purple',
    # 'gitlab_url': 'https://gitlab.com/gmdpapers/hydrothermalfoam'
}
html_context = {
    "menu_links": [
        (
            '<i class="fa fa-stamp"></i> Imprint',
            "https://www.hydrothermalfoam.info/manual/en/imprint.html",
        ),
        (
            '<i class="fa fa-envelope fa-fw"></i> Contact',
            "mailto:zguo@geomar.de",
            
        ),
        (
            '<img src="https://zenodo.org/static/img/orcid.png" height=14px> Zhikui Guo',
            'https://orcid.org/0000-0002-0604-0455'
        ),
        (
            '<i class="fa fa-book fa-fw"></i> LICENCE',
            "https://gitlab.com/gmdpapers/hydrothermalfoam/-/blob/master/LICENSE",
        ),
        (
            '<img src="https://img.shields.io/badge/Article-GMD-orange.svg" alt="Article">',
            'https://doi.org/10.5194/gmd-13-6547-2020'
        ),
        (
            '<i class="fa fa-gitlab fa-fw"></i> <img src="https://img.shields.io/badge/dynamic/json.svg?label=Source%20code&color=yellow&suffix=%20fetches&query=$.fetches.total&uri=https://gitlab.com/api/v4/projects/17901406/statistics?access_token=iyDQVx7ffDx9x-PrgRWz" alt="Source code fetches">',
            "https://gitlab.com/gmdpapers/hydrothermalfoam",
        ),
        (
            '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 256" fill="none" style="height: 14px;"> <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="256" height="256"> <path fill-rule="evenodd" clip-rule="evenodd" d="M181.534 254.252C185.566 255.823 190.164 255.722 194.234 253.764L246.94 228.403C252.478 225.738 256 220.132 256 213.983V42.0181C256 35.8689 252.478 30.2638 246.94 27.5988L194.234 2.23681C188.893 -0.333132 182.642 0.296344 177.955 3.70418C177.285 4.191 176.647 4.73454 176.049 5.33354L75.149 97.3862L31.1992 64.0247C27.1079 60.9191 21.3853 61.1735 17.5855 64.63L3.48936 77.4525C-1.15853 81.6805 -1.16386 88.9926 3.47785 93.2274L41.5926 128L3.47785 162.773C-1.16386 167.008 -1.15853 174.32 3.48936 178.548L17.5855 191.37C21.3853 194.827 27.1079 195.081 31.1992 191.976L75.149 158.614L176.049 250.667C177.645 252.264 179.519 253.467 181.534 254.252ZM192.039 69.8853L115.479 128L192.039 186.115V69.8853Z" fill="white"/> </mask> <g mask="url(#mask0)"> <path d="M246.94 27.6383L194.193 2.24138C188.088 -0.698302 180.791 0.541721 175.999 5.33332L3.32371 162.773C-1.32082 167.008 -1.31548 174.32 3.33523 178.548L17.4399 191.37C21.2421 194.827 26.9682 195.081 31.0619 191.976L239.003 34.2269C245.979 28.9347 255.999 33.9103 255.999 42.6667V42.0543C255.999 35.9078 252.478 30.3047 246.94 27.6383Z" fill="#0065A9"/> <g filter="url(#filter0_d)"> <path d="M246.94 228.362L194.193 253.759C188.088 256.698 180.791 255.458 175.999 250.667L3.32371 93.2272C-1.32082 88.9925 -1.31548 81.6802 3.33523 77.4523L17.4399 64.6298C21.2421 61.1733 26.9682 60.9188 31.0619 64.0245L239.003 221.773C245.979 227.065 255.999 222.09 255.999 213.333V213.946C255.999 220.092 252.478 225.695 246.94 228.362Z" fill="#007ACC"/> </g> <g filter="url(#filter1_d)"> <path d="M194.196 253.763C188.089 256.7 180.792 255.459 176 250.667C181.904 256.571 192 252.389 192 244.039V11.9606C192 3.61057 181.904 -0.571175 176 5.33321C180.792 0.541166 188.089 -0.700607 194.196 2.23648L246.934 27.5985C252.476 30.2635 256 35.8686 256 42.0178V213.983C256 220.132 252.476 225.737 246.934 228.402L194.196 253.763Z" fill="#1F9CF0"/> </g> <g style="mix-blend-mode:overlay" opacity="0.25"> <path fill-rule="evenodd" clip-rule="evenodd" d="M181.378 254.252C185.41 255.822 190.008 255.722 194.077 253.764L246.783 228.402C252.322 225.737 255.844 220.132 255.844 213.983V42.0179C255.844 35.8687 252.322 30.2636 246.784 27.5986L194.077 2.23665C188.737 -0.333299 182.486 0.296177 177.798 3.70401C177.129 4.19083 176.491 4.73437 175.892 5.33337L74.9927 97.386L31.0429 64.0245C26.9517 60.9189 21.229 61.1734 17.4292 64.6298L3.33311 77.4523C-1.31478 81.6803 -1.32011 88.9925 3.3216 93.2273L41.4364 128L3.3216 162.773C-1.32011 167.008 -1.31478 174.32 3.33311 178.548L17.4292 191.37C21.229 194.827 26.9517 195.081 31.0429 191.976L74.9927 158.614L175.892 250.667C177.488 252.264 179.363 253.467 181.378 254.252ZM191.883 69.8851L115.323 128L191.883 186.115V69.8851Z" fill="url(#paint0_linear)"/> </g> </g> <defs> <filter id="filter0_d" x="-21.4896" y="40.5225" width="298.822" height="236.149" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"> <feFlood flood-opacity="0" result="BackgroundImageFix"/> <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/> <feOffset/> <feGaussianBlur stdDeviation="10.6667"/> <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/> <feBlend mode="overlay" in2="BackgroundImageFix" result="effect1_dropShadow"/> <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/> </filter> <filter id="filter1_d" x="154.667" y="-20.6735" width="122.667" height="297.347" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"> <feFlood flood-opacity="0" result="BackgroundImageFix"/> <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/> <feOffset/> <feGaussianBlur stdDeviation="10.6667"/> <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/> <feBlend mode="overlay" in2="BackgroundImageFix" result="effect1_dropShadow"/> <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/> </filter> <linearGradient id="paint0_linear" x1="127.844" y1="0.659988" x2="127.844" y2="255.34" gradientUnits="userSpaceOnUse"> <stop stop-color="white"/> <stop offset="1" stop-color="white" stop-opacity="0"/> </linearGradient> </defs> </svg> <img src="https://img.shields.io/visual-studio-marketplace/i/zhikui.vscode-openfoam?color=blue&label=VS%20Code-OpenFOAM" alt="VS Code extension">',
            'https://marketplace.visualstudio.com/items?itemName=zhikui.vscode-openfoam'
        ),
        (
            '<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" viewBox="0 0 36 26" style="height: 14px;"><style type="text/css">.st0{fill:#21A9CC;}</style><path class="st0" d="M35.2,9.9c-0.8-0.6-2.7-0.8-4.1-0.5c-0.2-1.4-0.9-2.6-2.3-3.7L28,5.1l-0.5,0.8c-0.7,1.1-1,2.5-0.9,3.9c0,0.5,0.2,1.4,0.7,2.1c-0.5,0.3-1.5,0.6-2.7,0.6H0.2l-0.1,0.3c-0.2,1.4-0.2,5.8,2.5,9.2c2.1,2.6,5.2,3.9,9.3,3.9c8.8,0,15.4-4.2,18.5-11.9c1.2,0,3.8,0,5.1-2.6c0-0.1,0.1-0.2,0.3-0.7l0.1-0.3L35.2,9.9L35.2,9.9z M19.7,0h-3.7v3.5h3.7V0L19.7,0zM19.7,4.2h-3.7v3.5h3.7V4.2L19.7,4.2z M15.3,4.2h-3.7v3.5h3.7V4.2L15.3,4.2z M10.9,4.2H7.2v3.5h3.7V4.2L10.9,4.2z M6.5,8.4H2.8v3.5h3.7V8.4L6.5,8.4z M10.9,8.4H7.2v3.5h3.7V8.4L10.9,8.4z M15.3,8.4h-3.7v3.5h3.7V8.4L15.3,8.4z M19.7,8.4h-3.7v3.5h3.7V8.4L19.7,8.4zM24,8.4h-3.7v3.5H24V8.4L24,8.4z"></path></svg> <img src="https://img.shields.io/docker/pulls/zguo/hydrothermalfoam?label=Docker%20image%20pulls" alt="Docker image">',
            'https://hub.docker.com/r/zguo/hydrothermalfoam'
        ),
        (
            '<img src="https://pypi.org/static/images/logo-small.95de8436.svg" height=14px> <img src="https://img.shields.io/pypi/dm/scipyfoam?label=sciPyFoam" alt="sciPyFoam">',
            'https://pypi.org/project/sciPyFoam/'
        ),
        (
            '<img src="https://pypi.org/static/images/logo-small.95de8436.svg" height=14px> <img src="https://img.shields.io/pypi/dm/pyswEOS?label=pyswEOS" alt="sciPyFoam">',
            'https://pypi.org/project/pyswEOS/'
        ),
    ],
}

# favicon of the docs
html_favicon = "_static/favicon.png"
html_logo="_static/logo.png"
html_static_path = ['_static']
html_last_updated_fmt = '%b %d, %Y'
# If true, links to the reST sources are added to the pages.
html_show_sourcelink = False
# List of custom CSS files (needs sphinx>=1.8)
html_css_files = ["style.css"]

# Redefine supported_image_types for the HTML builder
from sphinx.builders.html import StandaloneHTMLBuilder
StandaloneHTMLBuilder.supported_image_types = [
  'image/svg+xml', 'image/png', 'image/jpeg', 'image/gif'
]


# -- Options for LaTeX output ---------------------------------------------
latex_engine = 'xelatex'
latex_elements = {
    'papersize': 'a4paper',
    'utf8extra': '',
    'inputenc': '',
    'babel': r'''\usepackage[english]{babel}''',
    'preamble': r'''\usepackage{ctex}
\definecolor{FOAM_green}{rgb}{0.27,0.49,0.36}
\setCJKsansfont{Source Han Sans CN} %为中文和英文设置特定字体
\setsansfont{Source Han Sans CN}
\setmainfont{Arial}
    ''',
}
# latex_logo='_static/logo.pdf'
# 设置公式和图片编号依赖于章节
numfig = True 
math_numfig = True
math_eqref_format = '({number})'
# 只对make latex有效
# numfig_format = 'Figure. %s'
numfig_secnum_depth = 1
imgmath_latex = 'dvilualatex'
imgmath_image_format = 'svg'
imgmath_dvipng_args = ['-gamma', '1.5', '-bg', 'Transparent']
# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'HydrothermalFoam.tex', 'HydrothermalFoam Manual',
     'Zhikui Guo, Lars Rüpke, Chunhui Tao', 'manual'),
]


# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'hydrothermalfoam', 'HydrothermalFoam Manual',
     [author], 1)
]


# -- Options for Texinfo output ----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, 'HydrothermalFoam', 'HydrothermalFoam Documentation',
     author, 'HydrothermalFoam', 'One line description of project.',
     'Miscellaneous'),
]

def setup(app):
    app.add_css_file("style.css")
    # app.add_javascript("js/custom.js")
    app.add_js_file(
        "https://cdn.jsdelivr.net/npm/clipboard@1/dist/clipboard.min.js")
    
# ====================================================================
# new defined cite style
from pybtex.style.formatting.unsrt import Style as UnsrtStyle
from pybtex.plugin import register_plugin
from collections import Counter
import re
import unicodedata

from pybtex.style.labels import BaseLabelStyle

_nonalnum_pattern = re.compile('[^A-Za-z0-9 \-]+', re.UNICODE)

def _strip_accents(s):
    return "".join(
        (c for c in unicodedata.normalize('NFD', s)
            if not unicodedata.combining(c)))

def _strip_nonalnum(parts):
    """Strip all non-alphanumerical characters from a list of strings.

    >>> print(_strip_nonalnum([u"ÅA. B. Testing 12+}[.@~_", u" 3%"]))
    AABTesting123
    """
    s = "".join(parts)
    return _nonalnum_pattern.sub("", _strip_accents(s))

class APALabelStyle(BaseLabelStyle):
    def format_labels(self, sorted_entries):
        labels = [self.format_label(entry) for entry in sorted_entries]
        count = Counter(labels)
        counted = Counter()
        for label in labels:
            if count[label] == 1:
                yield label
            else:
                yield label + chr(ord('a') + counted[label])
                counted.update([label])

    def format_label(self, entry):
        label = "Anonymous"
        if 'author' in entry.persons:
            label = self.format_author_or_editor_names(entry.persons['author'])
        elif 'editor' in entry.persons:
            label = self.format_author_or_editor_names(entry.persons['editor'])
        elif 'organization' in entry.fields:
            label = entry.fields['organization']
            if label.startswith("The "):
                label = label[4:]

        if 'year' in entry.fields:
            return "{}, {}".format(label, entry.fields['year'])
        else:
            return "{}, n.d.".format(label)

    def format_author_or_editor_names(self, persons):
        if len(persons) == 1:
            return _strip_nonalnum(persons[0].last_names)
        elif len(persons) == 2:
            return "{} & {}".format(
                _strip_nonalnum(persons[0].last_names),
                _strip_nonalnum(persons[1].last_names))
        else:
            return "{} et al.".format(
                _strip_nonalnum(persons[0].last_names))

class APAStyle(UnsrtStyle):

    default_label_style = APALabelStyle

register_plugin('pybtex.style.formatting', 'apa', APAStyle)
# # ====================================================================