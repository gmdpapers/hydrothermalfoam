.. HydrothermalFoam documentation front page

.. include:: /include.rst_


HydrothermalFoam Manual
=======================================

Welcome to the |foam| Manual! Here you will find resources for using HydrothermalFoam and examples of what
it can do.

.. tab:: Five minutes tutorial

   .. ifconfig:: language=='zh'

      .. raw:: html

         <div class="iframe-16x9">
            <iframe allowfullscreen width="560" height="315" frameborder="0" src="https://v.qq.com/txp/iframe/player.html?vid=p09599iy5yt"></iframe>
         </div>

   .. ifconfig:: language=='en'

      .. raw:: html

         <div class="iframe-16x9">
            <iframe allowfullscreen width="560" height="315" frameborder="0" src="https://www.youtube.com/embed/6czcxC90gp0?rel=0&amp;autoplay=0&mute=1"></iframe>
         </div>

.. tab:: Ridge Seminar Series Talk (5)

   .. ifconfig:: language=='zh'

      .. raw:: html

         <div class="iframe-16x9">
         <iframe allowfullscreen width="560" height="315" frameborder="0" src="https://www.youtube.com/embed/bLBkc9ikoYc?rel=0&amp;autoplay=0&mute=0"></iframe>
         </div>

   .. ifconfig:: language=='en'

      .. raw:: html

         <div class="iframe-16x9">
            <iframe allowfullscreen width="560" height="315" frameborder="0" src="https://www.youtube.com/embed/bLBkc9ikoYc?rel=0&amp;autoplay=0&mute=0"></iframe>
         </div>

.. tab:: Change log

   .. admonition:: What's new

      - [Mar. 2022] (50967d54_): Add |psFoam| and related manual (:ref:`hydrostaticpressure`)
      - [Oct. 2021] (f8572e49_): Add a :code:`linear2d` shape for the :code:`shape` of :code:`hydrothermalHeatFlux` boundary condition. see :numref:`installation_thermophysical` :ref:`installation_thermophysical` .
      - [Nov. 2020] (60232254_): HydrothermalFoam is now compatible with OpenFOAM-8! The only difference is the thermo-physical model. see :numref:`installation_thermophysical` :ref:`installation_thermophysical` .
      - [01 Dec. 2020] (a0419165_): Add access of **enthalpy** for IAPWS thermal-physical model. 
      - [04 Dec. 2020] (0c42a6c2_): Add :code:`cutoffT` option for IAPWS thermal-physical model to cutoff temperature to avoid temperature higher magmatic temperature of MOR when using :code:`hydrothermalHeatFlux` boundary condition. see below and :numref:`lst:thermophysicalProperties` for example.
   
      .. code-block::  foam
         :linenos:
         :emphasize-lines: 7

         mixture
         {
            specie
            {
               molWeight       18;
            }
            cutoffT 1000; // if temperature greater than 1000 K, cutoff it to 1000.
            porousMedia
            {
               porosity porosity [0 0 0 0 0 0 0] 0.1;
               kr kr [1 1 -3 -1 0 0 0] 2.0;
               cp_rock cp_rock [0 2 -2 -1 0 0 0] 880;
               rho_rock rho_rock [1 -3 0 0 0 0 0] 3000;
            }
         }

      - [16 Dec. 2020] (502c669e_): Update compilation of freesteam, see :ref:`installation_thermophysical_build_freesteam` 

.. toctree::
   :maxdepth: 3
   :caption: Contents
   :numbered:

   Introduction/index.rst
   Installation/index.rst
   Models_Equations/index.rst
   Tutorial/index.rst
   Cookbooks/index.rst
   refs.rst

.. toctree::
   :hidden:

   imprint.rst
