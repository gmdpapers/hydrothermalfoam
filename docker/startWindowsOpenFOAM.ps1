#----------------------------------*-PowerShell-*--------------------------------------
# =========                 |
# \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
#  \\    /   O peration     |
#   \\  /    A nd           | 
#    \\/     M anipulation  | Copyright (C) 2016-2019 OpenCFD Ltd.
#------------------------------------------------------------------------------
# License
#     This file is part of HydrothermalFoam.
#
# File
#     startMacHydrothermalFoam
#
# Description
#     1) Start HydrothermalFoam container with name 'hydrothermalfoam'
#     2) To login as root
#          * Root: to login as root run command "su root"
#           password: hydrothermalfoam
#
# NOTE:
#     This script should be placed in the home area 
#
#     To post-process, please install Paraview for your Windows seprately
#
#------------------------------------------------------------------------------

docker start  hydrothermalfoam
docker attach hydrothermalfoam
