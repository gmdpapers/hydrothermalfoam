.. _refs:

.. only:: html

    **************************
    References
    **************************

.. bibliography:: manual.bib
    :cited: 
    :style: apa 

    