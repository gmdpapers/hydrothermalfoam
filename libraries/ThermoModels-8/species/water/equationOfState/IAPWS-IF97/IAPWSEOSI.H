/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2015-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Note: modified from BoussinesqI.H
\*---------------------------------------------------------------------------*/

#include "IAPWSEOS.H"
// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template<class Specie>
inline Foam::IAPWSEOS<Specie>::IAPWSEOS
(
    const Specie& sp
)
:
    Specie(sp)
{}


template<class Specie>
inline Foam::IAPWSEOS<Specie>::IAPWSEOS
(
    const word& name,
    const IAPWSEOS<Specie>& b
)
:
    Specie(name, b)
{}


template<class Specie>
inline Foam::autoPtr<Foam::IAPWSEOS<Specie>>
Foam::IAPWSEOS<Specie>::clone() const
{
    return autoPtr<IAPWSEOS<Specie>>
    (
        new IAPWSEOS<Specie>(*this)
    );
}


template<class Specie>
inline Foam::autoPtr<Foam::IAPWSEOS<Specie>>
Foam::IAPWSEOS<Specie>::New
(
    const dictionary& dict
)
{
    return autoPtr<IAPWSEOS<Specie>>
    (
        new IAPWSEOS<Specie>(dict)
    );
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //
template<class Specie>
inline Foam::dimensionedScalar Foam::IAPWSEOS<Specie>::rho_rock
(
) const
{
    return rho_rock_;
}

template<class Specie>
inline Foam::dimensionedScalar Foam::IAPWSEOS<Specie>::cp_rock
(
) const
{
    return cp_rock_;
}

template<class Specie>
inline Foam::dimensionedScalar Foam::IAPWSEOS<Specie>::porosity
(
) const
{
    return porosity_;
}

template<class Specie>
inline Foam::dimensionedScalar Foam::IAPWSEOS<Specie>::kr
(
) const
{
    return kr_;
}

template<class Specie>
inline SteamState Foam::IAPWSEOS<Specie>::get_State_ph
(
    scalar p,
    scalar h
) const
{
    return freesteam_set_ph(p,h);
}

template<class Specie>
inline SteamState Foam::IAPWSEOS<Specie>::get_State_pT
(
    scalar p,
    scalar& T
) const
{
    double T0 = T;
    if(!CheckSetLim_pT(p,T0))FatalErrorInFunction<<"Check p, T range failed"<<abort(FatalError);
    if(T>m_cutoffT)T=m_cutoffT;
    return freesteam_set_pT(p,T0);
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::T_ph
(
    scalar p,
    scalar h
) const
{
    return T_ph(freesteam_set_ph(p,h)); 
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::T_ph
(
    SteamState S
) const
{
    return freesteam_T(S); 
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::rho
(
    scalar p,
    scalar T
) const
{
    return rho(freesteam_set_pT(p,T)); 
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::rho
(
    SteamState S
) const
{
    return freesteam_rho(S); 
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::rho_l
(
    scalar p,
    scalar T
) const
{
    scalar T0=T, p0=p;
    SteamState S=freesteam_set_pT(p,T);
    return rho_l(S);
}
template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::rho_l
(
    SteamState S
) const
{
    return freesteam_rho(S);
    // switch(S.region){
	// 	case 1:   //liquid
	// 		return 1./freesteam_region1_v_pT(S.R1.p,S.R1.T);
	// 	case 2:  //vapour
	// 		return 0;
	// 	case 3:  //supercritical regarded as liquid
	// 		return S.R3.rho;
	// 	case 4:
    //     {
    //         double rhol=0;
    //         if(S.R4.T < REGION1_TMAX){
    //             double psat = freesteam_region4_psat_T(S.R4.T);
    //             rhol = 1.0/freesteam_region1_v_pT(psat,S.R4.T);
    //         }else{
    //             rhol = freesteam_region4_rhof_T(S.R4.T);
    //         }
    //         return rhol;
    //     }
	// 	default:
    //         FatalErrorInFunction<<"ERROR: invalid region in freesteam_rho"<<S.region
    //         <<abort(FatalError);
	// }
    // return -1;
}
template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::rho_v
(
    scalar p,
    scalar T
) const
{
    scalar T0=T, p0=p;
    SteamState S=freesteam_set_pT(p,T);
    return rho_v(S);
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::rho_v
(
    SteamState S
) const
{
    return freesteam_rho(S);

    // switch(S.region){
	// 	case 1:   //liquid
	// 		return 0;
	// 	case 2:  //vapour
	// 		return 1./freesteam_region2_v_pT(S.R2.p,S.R2.T);
	// 	case 3:  //supercritical regarded as liquid
	// 		return 0;
	// 	case 4:
    //     {
    //         double rhov=0;
    //         if(S.R4.T < REGION1_TMAX){
    //             double psat = freesteam_region4_psat_T(S.R4.T);
    //             rhov = 1.0/freesteam_region2_v_pT(psat,S.R4.T);
    //         }else{
    //             rhov = freesteam_region4_rhog_T(S.R4.T);
    //         }
    //         return rhov;
    //     }
	// 	default:
    //         FatalErrorInFunction<<"ERROR: invalid region in freesteam_rho"<<S.region
    //         <<abort(FatalError);
	// }
    // return -1;
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::enthalpy
(
    scalar p,
    scalar T
) const
{
    return enthalpy(freesteam_set_pT(p,T)); 
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::enthalpy
(
    SteamState S
) const
{
    return freesteam_h(S); 
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::enthalpy_l
(
    scalar p,
    scalar T
) const
{
    scalar T0=T, p0=p;
    SteamState S=freesteam_set_pT(p,T);
    return enthalpy_l(S);
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::enthalpy_l
(
    SteamState S
) const
{
    return freesteam_h(S); 
    // int region=freesteam_region(S);
    // if(region==4) //two phase region
    // {
    //     double enthalpyl=0;
    //     double hf, hg;
    //     if(S.R4.T < REGION1_TMAX){
    //         double psat = freesteam_region4_psat_T(S.R4.T);
    //         hf = freesteam_region1_h_pT(psat,S.R4.T);
    //         hg = freesteam_region2_h_pT(psat,S.R4.T);
    //         //fprintf(stderr,"%s: T = %f K, psat = %f MPa, hf = %f kJ/kg, hg = %f kJ/kg\n",__func__,T,psat/1e6,hf/1e3,hg);
    //     }else{
    //         double rhof, rhog;
    //         rhof = freesteam_region4_rhof_T(S.R4.T);
    //         rhog = freesteam_region4_rhog_T(S.R4.T);
    //         hf = freesteam_region3_h_rhoT(rhof,S.R4.T);
    //         hg = freesteam_region3_h_rhoT(rhog,S.R4.T);
    //     }
    //     return hf;
    // }else // out of two phase region
    // {
    //     return freesteam_h(S);
    // }
}


template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::enthalpy_v
(
    scalar p,
    scalar T
) const
{
    scalar T0=T, p0=p;
    SteamState S=freesteam_set_pT(p,T);
    return enthalpy_v(S);
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::enthalpy_v
(
    SteamState S
) const
{
    return freesteam_h(S);

    // int region=freesteam_region(S);
    // if(region==4)
    // {
    //     double enthalpyl=0;
    //     double hf, hg;
    //     if(S.R4.T < REGION1_TMAX){
    //         double psat = freesteam_region4_psat_T(S.R4.T);
    //         hf = freesteam_region1_h_pT(psat,S.R4.T);
    //         hg = freesteam_region2_h_pT(psat,S.R4.T);
    //         //fprintf(stderr,"%s: T = %f K, psat = %f MPa, hf = %f kJ/kg, hg = %f kJ/kg\n",__func__,T,psat/1e6,hf/1e3,hg);
    //     }else{
    //         double rhof, rhog;
    //         rhof = freesteam_region4_rhof_T(S.R4.T);
    //         rhog = freesteam_region4_rhog_T(S.R4.T);
    //         hf = freesteam_region3_h_rhoT(rhof,S.R4.T);
    //         hg = freesteam_region3_h_rhoT(rhog,S.R4.T);
    //     }
    //     return hg;
    // }else
    // {
    //     return freesteam_h(S);
    // }
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::alphaP
(
    scalar p,
    scalar T
) const
{
    return alphaP(freesteam_set_pT(p,T)); 
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::alphaP
(
    SteamState S
) const
{
    return alphaP_pT(S); 
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::betaT
(
    scalar p,
    scalar T
) const
{
    return betaT(freesteam_set_pT(p,T)); 
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::betaT
(
    SteamState S
) const
{
    return betaT_pT(S); 
}

// thermo.he()调用的就是这个函数
template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::H(scalar p, scalar T) const
{
    return freesteam_h(freesteam_set_pT(p,T));
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::Cp(scalar p, scalar T) const
{
    // this is called by heHydroThermo.C -> Cp()
    double T0 = T;
    if(!CheckSetLim_pT(p,T0))FatalErrorInFunction<<"Check p, T range failed"<<abort(FatalError);
    return freesteam_cp(freesteam_set_pT(p,T0));
}


template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::E(scalar p, scalar T) const
{
    return 0;
}


template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::Cv(scalar p, scalar T) const
{
    return freesteam_cv(freesteam_set_pT(p,T)); 
}


template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::S
(
    scalar p,
    scalar T
) const
{
    return freesteam_s(freesteam_set_pT(p,T));
}


template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::psi
(
    scalar p,
    scalar T
) const
{
    return 0;
}


template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::Z
(
    scalar p,
    scalar T
) const
{
    return 0;
}


template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::CpMCv
(
    scalar p,
    scalar T
) const
{
    return 0;
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::Props_crit_p
(
    const scalar psat, 
    scalar& T_c, 
    scalar& h_lc, 
    scalar& h_vc, 
    scalar& rho_lc, 
    scalar& rho_vc) const
{
    // calculate liquid and vapour properties in two-phase region
    if(psat>IAPWS97_PCRIT)
    {
        T_c=-1;
        h_lc=-1;
        h_vc=-1;
        rho_lc=-1;
        rho_vc=-1;
        return -1;
    }else
    {
        T_c = freesteam_region4_Tsat_p(psat);
        h_lc = freesteam_region1_h_pT(psat, T_c);
        h_vc = freesteam_region2_h_pT(psat, T_c);
        rho_lc = 1.0/freesteam_region1_v_pT(psat,T_c);
        rho_vc = 1.0/freesteam_region2_v_pT(psat,T_c);
        // Info<<"p: "<<psat<<" T_c: "<<T_c<<" h_lc: "<<h_lc<<" h_vc: "<<h_vc<<" rho_lc: "<<rho_lc<<" rho_vc: "<<rho_vc<<endl;
    }
    return 0;
}
template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::H_pT (const scalar p, const scalar T, scalar& h_f, scalar& H_f) const
{
    SteamState S= freesteam_set_pT(p, T);
    h_f = freesteam_h(S);
    scalar rho_f =freesteam_rho(S);
    scalar H_r=porosityRhoCp_r*T;
    H_f = rho_f * h_f * m_porosity;
    return H_r + H_f;
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::Solve_T_bisection
(
    scalar& h, 
    const scalar H_t,
    const scalar p, 
    const scalar tol,
    const scalar iter_max,
    scalar Tup,
    scalar Tdown) const
{
    scalar H_f = 0;
    scalar h_f =0;
    // --------------double check-------------
    scalar Hup = H_pT(p, Tup,h_f, H_f);
    scalar Hdown = H_pT(p, Tdown,h_f, H_f);
    if(((Hup - H_t)*(Hdown-H_t))>0)
    {
        Info<<"Tup = "<<Tup<<" and Tdown = "<<Tdown<<" is not a proper bound for bisection method"<<endl;
        Info<<"Try to set Tup = IAPWS97_TMAX and Tdown = IAPWS97_TMIN"<<endl;
        Tup = IAPWS97_TMAX;
        Tdown = IAPWS97_TMAX;
    }
    // ---------------------------------------
    scalar Ttest = (Tup + Tdown)/2.0;
    scalar H = H_pT(p, Ttest,h_f, H_f);
    scalar iter = 0;
    while((mag(H-H_t)/H_f)>tol && (Tup-Tdown)>1e-4)
    {
        Hup = H_pT(p, Tup,h_f, H_f);
        Hdown = H_pT(p, Tdown,h_f, H_f);
        if((Hup - H_t)*(H-H_t)>0)
        {
            Tup = Ttest;
        }else
        {
            Tdown = Ttest;
        }
        Ttest = (Tup + Tdown)/2.0;
        H = H_pT(p, Ttest,h_f, H_f);
        iter++;
        if(iter>20)
        {
            Info<<"bisection iter: "<<iter<<" Tup: "<<Tup<<" Tdown: "<<Tdown<<" Tup-Tdown: "<<Tup-Tdown<<" Ttest: "<<Ttest<<" (mag(H-H_t)/H_f): "<<(mag(H-H_t)/H_f)<< endl;
        }
        if(iter>iter_max)
        {
            FatalErrorInFunction
            << "Thermal equilibrium failed to converge (bisection)" 
            << abort(FatalError);
        }
    }
    h=h_f;
    return Ttest;
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::Solve_T
(
    scalar& h, 
    const scalar H_t,
    const scalar p, 
    const scalar T0,
    const scalar Mass_f,
    const scalar tol,
    const scalar iter_max,
    const scalar Tguess) const
{
    // iteration
    scalar h_f = h, H_f = 0;
    scalar iter = 0;
    double Ttest = Tguess;
    if(!CheckLim_T(Ttest))FatalErrorInFunction<<"Check temperature range failed"<<abort(FatalError);  //check valid range
    scalar H = H_pT(p, Ttest, h_f, H_f);
    while((mag(H-H_t)/H_f)>tol)
    {
        // ------------
        double T1=Ttest-0.1, T2=Ttest+0.1;
        SteamState S1=freesteam_set_pT(p, T1);
        SteamState S2=freesteam_set_pT(p, T2);
        double H1=porosityRhoCp_r*freesteam_T(S1) + m_porosity*freesteam_rho(S1)*freesteam_h(S1) - H_t;
        double H2=porosityRhoCp_r*freesteam_T(S2) + m_porosity*freesteam_rho(S2)*freesteam_h(S2) - H_t;
        double dHdT = (H2-H1)/(T2-T1);
        // ------------
        // double dHdT = porosityRhoCp_r + m_porosity*rho_f*cp_f; //Newton-Raphson method, dh/dT=Cp
        Ttest = Ttest - (H-H_t)/dHdT;   //Newton-Raphson method
        if(!CheckLim_T(Ttest))FatalErrorInFunction<<"Check temperature range failed"<<abort(FatalError);  //check valid range
        H=H_pT(p, Ttest, h_f, H_f);

        iter++;
        if(iter>iter_max)
        {
            Info<<"Switch bisection method"<<endl;
            Ttest = Solve_T_bisection(h, H_t, p, tol, 100, Ttest+1, Ttest-1);
            break;
        }
    }
    h=h_f;
    return Ttest;
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::Solve_h_bisection
(
    scalar& h, 
    const scalar H_t,
    const scalar p, 
    const scalar tol,
    const scalar iter_max,
    scalar hup,
    scalar hdown) const
{
    scalar H_f = 0, h_f =0, Ttest = 0;
    // --------------double check-------------
    scalar Hup = H_ph(p, hup,Ttest, H_f);
    scalar Hdown = H_pT(p, hdown,Ttest, H_f);
    if(((Hup - H_t)*(Hdown-H_t))>0)
    {
        Info<<"Warning: hup = "<<hup<<" and hdown = "<<hdown<<" is not a proper bound for bisection method"<<endl;
        Info<<"Set to a much wider range"<<endl;
        hup = hup*1.1;
        hdown = hdown*0.9;
    }
    // ---------------------------------------
    scalar hTest = (hup + hdown)/2.0;
    scalar H = H_ph(p, hTest,Ttest, H_f);
    scalar iter = 0;
    while((mag(H-H_t)/H_f)>tol && (hup-hdown)>0.1)
    {
        Hup = H_ph(p, hup,Ttest, H_f);
        if((Hup - H_t)*(H-H_t)>0)
        {
            hup = hTest;
        }else
        {
            hdown = hTest;
        }
        hTest = (hup + hdown)/2.0;
        H = H_ph(p, hTest,Ttest, H_f);
        iter++;
        if(iter>0)
        {
            Info<<"bisection iter: "<<iter<<" hup: "<<hup<<" hdown: "<<hdown<<" hup-hdown: "<<hup-hdown<<" Ttest: "<<Ttest<<" H-H_t: "<<H-H_t<< endl;
        }
        if(iter>iter_max)
        {
            FatalErrorInFunction
            << "Thermal equilibrium failed to converge (bisection)" 
            << abort(FatalError);
        }
    }
    h=hTest;
    return Ttest;
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::H_ph (const scalar p, const scalar h, scalar& T, scalar& H_f) const
{
    SteamState S= freesteam_set_ph(p, h);
    T = freesteam_T(S);
    scalar rho_f =freesteam_rho(S);
    scalar H_r=porosityRhoCp_r*T;
    H_f = rho_f * h * m_porosity;
    return H_r + H_f;
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::Solve_h
(
    scalar& h, 
    const scalar H_t,
    const scalar p, 
    const scalar T0,
    const scalar Mass_f,
    const scalar tol,
    const scalar iter_max,
    const scalar Tguess) const
{
    // iteration
    // scalar h_f = h, H_f = 0;
    scalar hTest = h, Ttest = 0, H_f =0;
    scalar iter = 0;
    scalar H = H_ph(p, hTest, Ttest, H_f);
    if(!CheckLim_T(Ttest))FatalErrorInFunction<<"Check temperature range failed"<<abort(FatalError);  //check valid range
    
    while((mag(H-H_t)/H_f)>tol)
    {
        // ------------
        double h1=hTest-100, h2=hTest+100;
        SteamState S1=freesteam_set_ph(p, h1);
        SteamState S2=freesteam_set_ph(p, h2);
        double H1=porosityRhoCp_r*freesteam_T(S1) + m_porosity*freesteam_rho(S1)*freesteam_h(S1) - H_t;
        double H2=porosityRhoCp_r*freesteam_T(S2) + m_porosity*freesteam_rho(S2)*freesteam_h(S2) - H_t;
        double dHdh = (H2-H1)/(h2-h1);
        // ------------
        // double dHdT = porosityRhoCp_r + m_porosity*rho_f*cp_f; //Newton-Raphson method, dh/dT=Cp
        hTest = hTest - (H-H_t)/dHdh;   //Newton-Raphson method
        H=H_ph(p, hTest, Ttest, H_f);
        // Info<<"hTest: "<<hTest<<" Ttest: "<<Ttest<<" H: "<<H<<" H_t: "<<H_t<<" dHdh: "<<dHdh<<endl;
        if(!CheckLim_T(Ttest))FatalErrorInFunction<<"Check temperature range failed"<<abort(FatalError);  //check valid range
        
        // Info<<"iter: "<<iter<<" Ttest: "<<Ttest<<endl;
        iter++;
        if(iter>iter_max)
        {
            Info<<"Switch bisection method"<<endl;
            Ttest = Solve_h_bisection(h, H_t, p, tol, 100, hTest*1.001, hTest*0.009);
            break;
        }
    }
    h=hTest;
    return Ttest;
}

template<class Specie>
inline Foam::scalar Foam::IAPWSEOS<Specie>::ThermoEquilibrium
(
    scalar& h, 
    scalar& S_l,
    const scalar p, 
    const scalar T0,
    const scalar Mass_f,
    const scalar tol, //has default value 1e-8
    const scalar iter_max //has default value 100
) const
{
    // Info<<"p="<<p<<", h="<<h<<", mass="<<Mass_f<<", T0="<<T0<<endl;
    if(!CheckLim_p(p))FatalErrorInFunction<<"Check pressure range failed "<<abort(FatalError);
    scalar H_r=porosityRhoCp_r*T0;
    scalar H_f=m_porosity*h*Mass_f; 
    const scalar H_t=H_r + H_f;
    scalar T = 0;
    // calculate liquid and vapour properties in two-phase region
    scalar T_c, h_lc, h_vc, rho_lc, rho_vc;
    Props_crit_p(p, T_c, h_lc, h_vc, rho_lc, rho_vc);
    const scalar H_rc = porosityRhoCp_r*T_c;
    const scalar h_fc = (H_t - H_rc)/(m_porosity*Mass_f);
    if(h_fc>=h_lc && h_fc<=h_vc)
    {
        S_l = rho_vc*(h_vc - h_fc)/( h_fc*(rho_lc-rho_vc) - (h_lc*rho_lc-h_vc*rho_vc) );
        h=h_fc;
        T=T_c;
        Info<<"Two phase: S_l = "<<S_l<<" T: "<<T<<endl;
    }else
    {
        T = Solve_T(h, H_t, p, T0, Mass_f, tol, iter_max, T0);
        // T = Solve_h(h, H_t, p, T0, Mass_f, tol, iter_max, T0);
        // Calculate liquid mass fraction according to enthalpy
        SteamState S=freesteam_set_ph(p, h);
        if(S.region==1 || S.region==3)
        {
            S_l = 1;
        }else if(S.region==2)
        {
            S_l = 0;
        }else if(S.region==4)
        {
            FatalErrorInFunction<<"Into two phase region again"<<abort(FatalError);
        }else
        {
            FatalErrorInFunction<<"Unsupported freesteam region: "<<S.region
            <<abort(FatalError);
        }
    }
    if(mag(T-T0)>5)
    {
        FatalErrorInFunction
            << "温度变化太大(Temperature difference greater than 5)" 
            <<endl;
            // << abort(FatalError);
    }
    return T;
}


// * * * * * * * * * * * * * * * Member Operators  * * * * * * * * * * * * * //

template<class Specie>
inline void Foam::IAPWSEOS<Specie>::operator=
(
    const IAPWSEOS<Specie>& b
)
{
    Specie::operator=(b);
}


template<class Specie>
inline void Foam::IAPWSEOS<Specie>::operator+=
(
    const IAPWSEOS<Specie>& b
)
{
    scalar Y1 = this->Y();
    Specie::operator+=(b);

    if (mag(this->Y()) > small)
    {
        Y1 /= this->Y();
        const scalar Y2 = b.Y()/this->Y();
    }
}


template<class Specie>
inline void Foam::IAPWSEOS<Specie>::operator*=(const scalar s)
{
    Specie::operator*=(s);
}


// * * * * * * * * * * * * * * * Friend Operators  * * * * * * * * * * * * * //

template<class Specie>
inline Foam::IAPWSEOS<Specie> Foam::operator+
(
    const IAPWSEOS<Specie>& b1,
    const IAPWSEOS<Specie>& b2
)
{
    Specie sp(static_cast<const Specie&>(b1) + static_cast<const Specie&>(b2));

    if (mag(sp.Y()) < small)
    {
        return IAPWSEOS<Specie>
        (
            sp
        );
    }
    else
    {
        const scalar Y1 = b1.Y()/sp.Y();
        const scalar Y2 = b2.Y()/sp.Y();

        return IAPWSEOS<Specie>
        (
            sp
        );
    }
}


template<class Specie>
inline Foam::IAPWSEOS<Specie> Foam::operator*
(
    const scalar s,
    const IAPWSEOS<Specie>& b
)
{
    return IAPWSEOS<Specie>
    (
        s*static_cast<const Specie&>(b)
    );
}


template<class Specie>
inline Foam::IAPWSEOS<Specie> Foam::operator==
(
    const IAPWSEOS<Specie>& b1,
    const IAPWSEOS<Specie>& b2
)
{
    Specie sp(static_cast<const Specie&>(b1) == static_cast<const Specie&>(b2));

    const scalar Y1 = b1.Y()/sp.Y();
    const scalar Y2 = b2.Y()/sp.Y();

    return IAPWSEOS<Specie>
    (
        sp
    );
}


// ************************************************************************* //
