.. include:: /include.rst_

.. _sec:2Dbox:

Convection in a box
=====================


In this first example, let us consider a simple situation: 
a 2d box that is heated from below, 
insulated at the side walls, 
and cooled from the top. 
We will start from a 2D box example and then make some changes to show its features.

.. toctree::
   :maxdepth: 1

   helloworld.rst
   nonuniformFixedValueBC.rst
   timeDependentPerm.rst
   gmsh.rst
   3Dbox.rst
   parallelComputing.rst
