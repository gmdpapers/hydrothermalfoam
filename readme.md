[![Generic badge](https://img.shields.io/badge/Doc-Manual-<COLOR>.svg)](https://gmdpapers.gitlab.io/hydrothermalfoam/manual/)
[![Generic badge](https://img.shields.io/badge/Doc-Documentation-<COLOR>.svg)](https://gmdpapers.gitlab.io/hydrothermalfoam/doxygen/)
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](LICENSE)
[![](https://img.shields.io/docker/pulls/zguo/hydrothermalfoam?label=Docker%20image%20pulls)](https://hub.docker.com/r/zguo/hydrothermalfoam)
[![](https://img.shields.io/visual-studio-marketplace/i/zhikui.vscode-openfoam?label=VS%20Code-OpenFOAM)](https://marketplace.visualstudio.com/items?itemName=zhikui.vscode-openfoam)
[![](https://img.shields.io/badge/dynamic/json.svg?label=Source%20code&color=yellow&suffix=%20fetches&query=$.fetches.total&uri=https://gitlab.com/api/v4/projects/17901406/statistics?access_token=iyDQVx7ffDx9x-PrgRWz)](https://gitlab.com/gmdpapers/hydrothermalfoam)
[![](https://img.shields.io/badge/Publication-10.5194%2Fgmd--13--6547--2020-important)](https://doi.org/10.5194/gmd-13-6547-2020)

# About HydrothermalFOAM
**HydrothermalFoam** —combination of **hydrothermal** and **OpenFOAM** — 
a three dimensional hydro-thermo-transport model designed to resolvefluid flow within submarine hydrothermal circulation systems. 
HydrothermalFoam has been developed on the OpenFOAM platform, 
which is a Finite Volume based C++ toolbox for fluid-dynamic simulations 
and for developing customized numerical solvers that provides access to 
state-of-the-art parallelized solvers and to a wide range of pre- and post-processing tools. 
We have implemented a porous media Darcy-flow model with associated boundary conditions designed to facilitate numerical 
simulations of submarine hydrothermal systems. 
The current implementation is valid for single-phase fluid states and uses a pure water equation-of-state (IAPWS-97). 
We here present the model formulation, OpenFOAM implementation details, and a sequence of 1-D, 2-D and 3-D benchmark tests. 
The source code repository further includes a number of tutorials that canbe used as starting points 
for building specialized hydrothermal flow models. 

# [Download and installation instructions](https://www.hydrothermalfoam.info/manual/en/Installation/index.html)
# Documentation: [English version](https://www.hydrothermalfoam.info/manual/en/index.html),  [Chinese version](https://www.hydrothermalfoam.info/manual/zh/index.html)
# [Quick start video tutorial](https://youtu.be/6czcxC90gp0)
# [Source code documentation](https://gmdpapers.gitlab.io/hydrothermalfoam/doxygen/)
# [Reporting bugs in HydrothermalFOAM](https://gitlab.com/gmdpapers/hydrothermalfoam/-/issues)

# [HydrothermalFOAM environment docker image](https://hub.docker.com/r/zguo/hydrothermalfoam)

# How to cite

* [Endnote](https://gmd.copernicus.org/articles/13/6547/2020/gmd-13-6547-2020.ris)

* [BibTex entry](https://gmd.copernicus.org/articles/13/6547/2020/gmd-13-6547-2020.bib)

```
@Article{guo2020hydrothermalfoam,
AUTHOR = {Guo, Z. and R\"upke, L. and Tao, C.},
TITLE = {\textit{HydrothermalFoam} v1.0: a 3-D hydrothermal transport model \hack{\break} for natural submarine hydrothermal systems},
JOURNAL = {Geoscientific Model Development},
VOLUME = {13},
YEAR = {2020},
NUMBER = {12},
PAGES = {6547--6565},
URL = {https://gmd.copernicus.org/articles/13/6547/2020/},
DOI = {10.5194/gmd-13-6547-2020}
}
```

# Licence
HydrothermalFoam is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.  See the [LICENSE](./LICENSE).