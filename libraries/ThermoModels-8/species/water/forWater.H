/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2020 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#ifndef forWate_H
#define forWate_H

#include "specie.H"

#include "IAPWSEOS.H"

// #include "hConstThermo.H"
// // #include "janafThermo.H"
#include "IAPWSThermo.H"

//TODO: add enthalpy energy according what implemented in the V7 version, for multiphase study
// #include "specificEnthalpy.H"
#include "temperatureEnergy.H"

#include "IAPWSTransport.H"

#include "thermo.H"

#include "forThermo.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#define forWaterEquations(Mu, He, Cp, Macro, Args...)                            \
    forThermo(Mu, He, Cp, IAPWSEOS, specie, Macro, Args)

#define forWaterEnergiesAndThermos(Mu, Macro, Args...)                           \
    forWaterEquations(Mu, temperatureEnergy, IAPWSThermo, Macro, Args);         \
    // forWaterEquations(Mu, specificEnthalpy, IAPWSThermo, Macro, Args)

#define forWaterTransports(Macro, Args...)                                       \
    forWaterEnergiesAndThermos(IAPWSTransport, Macro, Args)

#define forWater(Macro, Args...)                                               \
    forWaterTransports(Macro, Args)
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
