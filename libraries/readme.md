
## Cross version

`libraries/ThermoModels-8` is for OpenFOAM version 8.

For OF-7 and OF-8, the only difference is the thermophysical model, `libraries/ThermoModels` is for OF-7 users, and `libraries/ThermoModels-8` is for OF-8 users.

## Build

- makeFreeSteam: build freesteam with the local gsl source code

- Allmake: make freesteam, BoundaryConditions and ThermoModels 

- cleanFreeSteam: clean build directory of freesteam and gsl, remove GSL_ROOT_DIR which is defined in `makeFreeSteam` and remove $FOAM_USER_LIBBIN/libfreesteam.*

- Allclean: clean freesteam , BoundaryConditions and ThermoModels  