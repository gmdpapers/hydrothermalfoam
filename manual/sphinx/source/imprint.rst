Imprint
===========

.. admonition:: Imprint

    This website contains privately compiled manual, documentation and source code of open-source code of HydrothermalFoam which is developed to resolve hydrothermal convection problem of submarine hydrothermal systems. Responsible for the content is Zhikui Guo.

    **Contact**:

    * Zhikui Guo

    * c/o GEOMAR Helmholtz Centre for Ocean Research Kiel

    * Wischhofstr. 1-3

    * 24148 Kiel

    * Germany

    * Email: zguoe@geomar.de