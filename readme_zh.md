> This is only for the source code owner, users please ignore this readme file!


# Docker容器的制作和发布

## 尝试在Mac上通过Docker使用OpenFOAM
按照OpenFOAM官方的在[Mac OS平台的安装指南](https://openfoam.org/download/7-macos/),

1. **安装docker** ：
这一步很简单，直接下载mac版的[docker安装文件](https://docs.docker.com/docker-for-mac/install/)，双击安装。
安装完成后在桌面顶部的工具栏里面会显示docker的图标，
点击图标可以看到`Docker Desktop is running`的绿色状态，表示已经安装成功且已经在运行了。
以下有几个常用的命令可以在终端直接使用：
```bash
# 1. check docker version
docker --version
# 2. pull docker image from dockerhub
docker image pull library/hello-world
# 3. check available local images
docker image ls
docker image rm [imageName] # remove a docker
# 4. run a container from a image
docker container run hydrothermalfoam
# 5. check information of container
docker container ls
docker container ls --all  #list all the containers
# 6. terminate a container
docker container kill [container id]
```

2. **安装openfoam7-macos**

```
curl --create-dirs -o ~/OpenFOAM_Docker/openfoam7-macos http://dl.openfoam.org/docker/openfoam7-macos
chmod 755 ~/OpenFOAM_Docker/openfoam7-macos 
```

3. **设置Xquartz**
因为OpenFOAM运行的ubuntu镜像的容器，可视化界面是依托Xquartz的，所以需要安装最新版的Xquartz，并且需要做如下设置
* Start XQuartz.
* Select XQuartz → Preferences → Security
* Check “Allow connections from network clients” and “Authenticate connections”.
* Shut down and Restart XQuartz.

4. **创建镜像**

```bash
curl -o ~/OpenFOAM_Docker/openfoam-macos-file-system http://dl.openfoam.org/docker/openfoam-macos-file-system
chmod 755 ~/OpenFOAM_Docker/openfoam-macos-file-system
```
对镜像的操作
```bash
openfoam-macos-file-system -s 20 create #: creates a 20GB file system.
openfoam-macos-file-system mount #: mounts the file system.
openfoam-macos-file-system unmount #: unmounts the file system.
openfoam-macos-file-system automount #: mounts the file system, automatically on machine boot.
openfoam-macos-file-system delete #: deletes the file system permanently, losing all data.
```

## 制作docker容器: 在官方容器基础上修改提交

1. 基于官方OpenFOAM镜像创建一个容器，并命名该容器为`openfoam`
```
docker run -it -d --name openfoam openfoam/openfoam7-paraview56
```
2. 以root用户运行容器
```
docker exec -u 0 -it openfoam /bin/bash
```

3. 查看用户名并修改密码
```
apt-get update 
apt-get install sudo
cat /etc/passwd| cut -f 1 -d :
passwd openfoam
chmod 666 /etc/sudoers
vim /etc/sudoers  # openfoam ALL=(ALL) ALL
chmod 444 /etc/sudoers
```

现在可以以普通用户身份运行容器了，`docker exec openfoam`，默认的用户名为openfoam，在里面可以用sudo命令了

3. 安装ohmyzsh

```
apt-get install zsh
apt-get install git
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
sudo chown -R openfoam:openfoam /home/openfoam/.zsh_history
```

4. 打包新镜像
```
docker tag hydrothermalfoam zguo/hydrothermalfoam:1.0 #打标签
docker commit openfoam hydrothermalfoam #生成镜像
docker push zguo/hydrothermalfoam:1.0 #上传
```

# Build

**build solvers**
1. `cd solvers`
2. `./Allwmake`

**build libraries**
1. `cd libraries`
2. `./Allmake`


## Solvers

* HydrothermalSinglePhaseDarcyFoam

## libraries
### BoundaryConditions
* HydrothermalHeatFlux
* HydrothermalMassFluxPressure
* SubmarinePressure
* noFlux

### Thermophysical Models
* water

example of **thermophysicalProperties** file

```c++
thermoType
{
    type            htHydroThermo;
    mixture         pureMixture;
    transport       IAPWS;
    thermo          IAPWS;
    equationOfState IAPWS;
    specie          specie;
    energy          temperature;
}

mixture
{
    specie
    {
        molWeight       18;
    }
}
```

* simple

```c++
thermoType
{
    type            htHydroThermo;
    mixture         pureMixture;
    transport       const;
    thermo          const;
    equationOfState Boussinesq;
    specie          specie;
    energy          temperature; //sensibleEnthalpy, temperature
}

mixture
{
    specie
    {
        molWeight       18;
    }
    equationOfState
    {
        rho0            1000;
        T0              273.15;
        beta            0.001;
    }
    thermodynamics
    {
        Cp          5100;
    }
    transport
    {
        mu          0.0001;
    }
}
```

# Debug
The crash problem are mostly due to complex number, e.g. using `sqrt` to calculate squart root of a negative value. negative value to `log`, etc.