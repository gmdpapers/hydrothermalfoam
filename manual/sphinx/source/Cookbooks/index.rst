.. _index_cookbooks:

*******************
Cookbooks
*******************
 
.. include:: /include.rst_

.. only:: html 

   **Connections between tutorial programs**

   The following graph shows the connections between cookbook cases. 
   Click on any of the boxes to go to one of the cases. 
   If you hover your mouse pointer over a box, a brief description of the cookbook example should appear.
   And if you click a box, it links to the GitLab repository of the cookbook case.

   .. graphviz:: /../figures/dot/map_cookbooks.dot
      :align: center

.. toctree::
   :maxdepth: 2

   box/index.rst
   pipe/index.rst
   singlePass/index.rst