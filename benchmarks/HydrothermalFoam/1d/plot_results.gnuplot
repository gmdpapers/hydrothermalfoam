set terminal qt size 900,1200
# set terminal pdf size 10,6
# set output "sales_data.pdf"

set multiplot layout 3, 2 
# set bmargin 20
set yrange [149:350]
set ytics 150, 50
set tics font ",20" 
set ytics textcolor "red"
set ytics nomirror
set title font ",20"
set ylabel "Temperature (℃)" offset -2.5 font ",20" textcolor "red"
unset key

# subfigure (A)
set lmargin 10
unset xlabel
unset xtics
set title "Horizontal without gravity"
set label "(A)" at graph 0.02, 0.9 font ",20"
set label "250 years" at graph 0.7, 0.9 font ",20"
plot  "<awk 'NR>0 {print $1/1000,$2-273.15}' h1/postProcessing/linesample/7884000000/data_T_p.xy" with lines lw 2 lc "red" axis x1y1 , \
      "<awk 'NR>0 {print $1/1000,$3/1e6}' h1/postProcessing/linesample/7884000000/data_T_p.xy" with lines lw 2 lc "blue" notitle axis x1y2, \
      # "<awk 'NR>1 {print $1/1000,$2}' HYDROTHERMAL/h1/dx10_T_p_250.0.txt" with lines lw 2 lc "black" dt "." axis x1y1 , \
      # "<awk 'NR>1 {print $1/1000,$3/10}' HYDROTHERMAL/h1/dx10_T_p_250.0.txt" with lines lw 2 lc "black" dt "." notitle axis x1y2 

# subfigure (B)
set title "Vertical with gravity"
unset ylabel
unset ytics
set lmargin 0
unset xlabel
unset xtics
set y2label "Pressure (MPa)" offset 2.5 font ",20" textcolor "blue"
set y2range [25:50]
set y2tics 25, 5
set y2tics textcolor "blue"
unset label
set label "(B)" at graph 0.02, 0.9 font ",20"
set label "750 years" at graph 0.7, 0.9 font ",20"
plot  "<awk 'NR>0 {print $1/1000,$2-273.15}' v1/postProcessing/linesample/23652000000/data_T_p.xy" with lines lw 2 lc "red" axis x1y1 , \
      "<awk 'NR>0 {print $1/1000,$3/1e6}' v1/postProcessing/linesample/23652000000/data_T_p.xy" with lines lw 2 lc "blue" notitle axis x1y2, \
      # "<awk 'NR>1 {print $1/1000,$2}' HYDROTHERMAL/v1/dx10_T_p_750.0.txt" with lines lw 2 lc "black" dt "." axis x1y1 , \
      # "<awk 'NR>1 {print $1/1000,$3/10}' HYDROTHERMAL/v1/dx10_T_p_750.0.txt" with lines lw 2 lc "black" dt "." notitle axis x1y2 

# subfigure (C)
set lmargin 10
unset label
unset y2label
unset y2tics
unset title
unset xlabel
unset xtics
set yrange [290:455]
set y2range [20:40]
set ytics 300, 50
set ytics font ",20"
set ytics textcolor "red"
set ytics nomirror
set ylabel "Temperature (℃)" offset -2.5 font ",20" textcolor "red"
set label "(C)" at graph 0.02, 0.9 font ",20"
set label "120 years" at graph 0.7, 0.9 font ",20"
plot  "<awk 'NR>0 {print $1/1000,$2-273.15}' h2/postProcessing/linesample/3784320000/data_T_p.xy" with lines lw 2 lc "red" notitle axis x1y1 , \
      "<awk 'NR>0 {print $1/1000,$3/1e6}' h2/postProcessing/linesample/3784320000/data_T_p.xy" with lines lw 2 lc "blue" notitle axis x1y2, \
      # "<awk 'NR>1 {print $1/1000,$2}' HYDROTHERMAL/h2/dx10_T_p_120.0.txt" with lines lw 2 lc "black" dt "." axis x1y1 , \
      # "<awk 'NR>1 {print $1/1000,$3/10}' HYDROTHERMAL/h2/dx10_T_p_120.0.txt" with lines lw 2 lc "black" dt "." notitle axis x1y2 

# subfigure (D)
unset ylabel
unset ytics
set lmargin 0
unset xlabel
unset xtics
set y2label "Pressure (MPa)" offset 2.5 font ",20" textcolor "blue"
set y2tics 20, 5
set y2tics font ",20"
set y2tics textcolor "blue"
unset label
set label "(D)" at graph 0.02, 0.9 font ",20"
set label "350 years" at graph 0.7, 0.9 font ",20"
plot  "<awk 'NR>0 {print $1/1000,$2-273.15}' v2/postProcessing/linesample/11037600000/data_T_p.xy" with lines lw 2 lc "red" axis x1y1 , \
      "<awk 'NR>0 {print $1/1000,$3/1e6}' v2/postProcessing/linesample/11037600000/data_T_p.xy" with lines lw 2 lc "blue" notitle axis x1y2 ,\
      # "<awk 'NR>1 {print $1/1000,$2}' HYDROTHERMAL/v2/dx10_T_p_350.0.txt" with lines lw 2 lc "black" dt "." axis x1y1 , \
      # "<awk 'NR>1 {print $1/1000,$3/10}' HYDROTHERMAL/v2/dx10_T_p_350.0.txt" with lines lw 2 lc "black" dt "." notitle axis x1y2 

# subfigure (E)
set lmargin 10
unset label
unset y2label
unset y2tics
unset title
set yrange [290:510]
set y2range [0:15]
set ytics 300, 50
set tics font ",20"
set ytics textcolor "red"
set ytics nomirror
set xlabel "X (km)" font ",20"
set xtics 0, 0.5
set ylabel "Temperature (℃)" offset -2.5 font ",20" textcolor "red"
set label "(E)" at graph 0.02, 0.9 font ",20"
set label "1500 years" at graph 0.7, 0.9 font ",20"
plot  "<awk 'NR>0 {print $1/1000,$2-273.15}' h3/postProcessing/linesample/47304000000/data_T_p.xy" with lines lw 2 lc "red" axis x1y1 , \
      "<awk 'NR>0 {print $1/1000,$3/1e6}' h3/postProcessing/linesample/47304000000/data_T_p.xy" with lines lw 2 lc "blue" notitle axis x1y2, \
      # "<awk 'NR>1 {print $1/1000,$2}' HYDROTHERMAL/h3/dx10_T_p_1500.0.txt" with lines lw 2 lc "black" dt "." axis x1y1 , \
      # "<awk 'NR>1 {print $1/1000,$3/10}' HYDROTHERMAL/h3/dx10_T_p_1500.0.txt" with lines lw 2 lc "black" dt "." notitle axis x1y2 

# subfigure (F)
unset ylabel
unset ytics
set lmargin 0
set xlabel "X (km)" font ",20"
set xtics 0, 0.5
set y2label "Pressure (MPa)" offset 2.5 font ",20"  textcolor "blue"
set y2tics 0, 5
set y2tics textcolor "blue"
unset label
set label "(F)" at graph 0.02, 0.9 font ",20"
set label "1500 years" at graph 0.7, 0.9 font ",20"
plot  "<awk 'NR>0 {print $1/1000,$2-273.15}' v3/postProcessing/linesample/47304000000/data_T_p.xy" with lines lw 2 lc "red" axis x1y1 , \
      "<awk 'NR>0 {print $1/1000,$3/1e6}' v3/postProcessing/linesample/47304000000/data_T_p.xy" with lines lw 2 lc "blue" notitle axis x1y2 ,\
      # "<awk 'NR>1 {print $1/1000,$2}' HYDROTHERMAL/v3/dx10_T_p_1500.0.txt" with lines lw 2 lc "black" dt "." axis x1y1 , \
      # "<awk 'NR>1 {print $1/1000,$3/10}' HYDROTHERMAL/v3/dx10_T_p_1500.0.txt" with lines lw 2 lc "black" dt "." notitle axis x1y2 
