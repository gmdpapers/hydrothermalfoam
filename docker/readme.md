There are two bash script files for precompiled [HydrothermalFoam](https://hub.docker.com/repository/docker/zguo/hydrothermalfoam) docker image installation and container running. 

# MacOS
Run the script files in terminal.

1. Install
```
./installMacHydrothermalFoam.sh
```
The working directory on host is `~/HydrothermalFoam_run`, in container is `/home/openfoam/HydrothermalFoam_run`.

2. Start runing
```
./startMacOpenFOAM.sh
```

# Video tutorial of quick starting

[![Video: 5 minutes quick start](https://img.youtube.com/vi/6czcxC90gp0/0.jpg)](https://youtu.be/6czcxC90gp0)