xmin=-4500;
xmax=4500;
ymin=-3000;
ymax=0;
zmin=-100;
zmax=0;
width_heatsource=1000;
x0_heatsource=0;
lc=100;
lc_c=100;
Point(1) = {xmin, ymax, zmax, lc};
Point(2) = {xmax, ymax, zmax, lc};
Point(3) = {xmax, ymin, zmax, lc};
Point(4) = {xmin, ymin, zmax, lc};
Point(5) = {x0_heatsource-width_heatsource/2.0, ymin, zmax, lc_c};
Point(6) = {x0_heatsource+width_heatsource/2.0, ymin, zmax, lc_c};
Point(7) = {x0_heatsource-width_heatsource/2.0, ymax, zmax, lc_c};
Point(8) = {x0_heatsource+width_heatsource/2.0, ymax, zmax, lc_c};

Line(1) = {4, 5};
//+
Line(2) = {5, 6};
//+
Line(3) = {6, 3};
//+
Line(4) = {3, 2};
//+
Line(5) = {2, 8};
//+
Line(6) = {8, 7};
//+
Line(7) = {7, 1};
//+
Line(8) = {1, 4};
//+
Line(9) = {5, 7};
//+
Line(10) = {8, 6};
//+
Line Loop(1) = {7, 8, 1, 9};
//+
Plane Surface(1) = {1};
//+
Line Loop(2) = {9, -6, 10, -2};
//+
Plane Surface(2) = {2};
//+
Line Loop(3) = {10, 3, 4, 5};
//+
Plane Surface(3) = {3};

// Transfinite Surface {1,2,3};
// Recombine Surface {1,2,3};


Extrude {0, 0, zmin} {
Surface{1,2,3};
Layers{1};
Recombine;
}
Physical Volume("internal") = {1,2,3};

Physical Surface("frontAndBack") = {1,2,3,32,54,76};
Physical Surface("bottom") = {27,67};
Physical Surface("left") = {23};
Physical Surface("top") = {19,45,75};
Physical Surface("right") = {71};
Physical Surface("heatsource") = {53};
//+

