function cleanCase()
{
    caseDir=$1
    cd ${caseDir}/2d && ./clean.sh && cd ../2d_par && ./clean.sh && cd ../../
}

cleanCase fixedGradient_T
cleanCase fixedHeatFlux
cleanCase fixedTfixedFluxPressure
cleanCase fixedTnoFlux