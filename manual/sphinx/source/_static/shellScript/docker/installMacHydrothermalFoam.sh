#----------------------------------*-sh-*--------------------------------------
# =========                 |
# \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
#  \\    /   O peration     |
#   \\  /    A nd           | Copyright (C) 2016-2019 OpenCFD Ltd. 
#    \\/     M anipulation  |
#------------------------------------------------------------------------------
# License
#     This file is part of HydrothermalFoam.
#
# File
#     installMacHydrothermalFoam
#
# Description
#     this script os for native build of DOCKER on MAC-OSX
#     1) Pull HydrothermalFoam from docker hub if it does not exist in local
#        environment 
#     2) Create a container with the name hydrothermalfoam
#     3) To login as root
#          *  Root: to login as root run command "su root"
#          password: hydrothermalfoam
#
# NOTE:
#     This script should be placed in home area
#     To post-process, please install Paraview on your MAC
#
#------------------------------------------------------------------------------

homeInHost="${1:-$HOME}"
dirInContainer="/home/openfoam/HydrothermalFoam_runs"
dirInHost="${homeInHost}/HydrothermalFoam_runs"
imageName="zguo/hydrothermalfoam"
containerName="hydrothermalfoam"
# List container in docker environment
echo "*********************************************************"
echo "List of Container in docker environment:"
echo "*********************************************************"
docker ps -a 
echo "**************************************** "
echo "			"
echo "Creating Docker OpenFOAM container ${containerName}"
# Create docker container with OpenFOAM environment
docker run  -it -d --name ${containerName} --workdir="/home/openfoam" -v=${dirInHost}:${dirInContainer} -e DISPLAY=host.docker.internal:0 ${imageName}
echo "Container ${containerName} was created."
echo "*****************************************************************************"
echo "Run the `docker start hydrothermalfoam`  command to start the container"
echo "*****************************************************************************"
