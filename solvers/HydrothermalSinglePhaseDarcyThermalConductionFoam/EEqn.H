/**
 * @brief jdsjajdsads 
 * 
 * \f$ E = mc^2 \f$
 * 
 */
if (thermo.he().name()=="T")
{
    fvScalarMatrix TEqn
    (
        // (porosity*rho*Cp+(1.0-porosity)*rho_rock*cp_rock)*fvm::ddt(T)
        // // +fvm::div(phi*fvc::interpolate(Cp),T)
        rho_rock*cp_rock*fvm::ddt(T)
        == 
        fvm::laplacian(kr,T)
        // +
        // fvm::Sp(fvc::div(phi*fvc::interpolate(Cp)),T)
        // +
        // mu/permeability*magSqr(U)
        // +
        // fvm::Sp(alphaP*(porosity*fvc::ddt(p)+(U & fvc::grad(p))),T)
        // dLnRho_dLnT= T*alphaP
    );

    TEqn.solve();
    // T.correctBoundaryConditions();
}else
{
    FatalErrorInFunction
            << "The energy equation in terms of enthalpy is still under developing\n" 
            << "Please use keyword of temperture for energy entry in thermophysicalProperties."
            << abort(FatalError);
}