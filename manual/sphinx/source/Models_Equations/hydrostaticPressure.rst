.. _hydrostaticpressure:

******************************************
Hydrostatic pressure
******************************************

.. include:: /include.rst_


Equation
=======================

The hydrostatic pressure is defined as :eq:`eq:p_static`,

.. math::
    :label: eq:p_static

    \nabla p_{static} =\rho \vec{g}

Implementation formula
==========================

Unfortunately, the OpenFOAM doesn't have the implicit operator of :code:`fvm::grad()`. 
But we can mathematically play a little bit with :eq:`eq:p_static` and make it work in OpenFOAM. 

* Construct a divergence formula of :math:`p_{static}`

.. math::
    :label: eq:p_static:div

    \nabla \cdot (\vec{n}p_{static}) = \vec{n}\cdot \nabla p_{static} + p_{static}\nabla \vec{n}

Where :math:`\vec{n}` is the normal vector. so :math:`\nabla \vec{g}` is zero, 
therefore we can get the final expression of the hydrostatic pressure in terms of a flux format by combing :eq:`eq:p_static:div` and :eq:`eq:p_static`, 

.. math::
    :label: eq:p_static:implements

    \nabla \cdot (\vec{n}p_{static}) = \rho ||\vec{g}||


OpenFOAM implementation
===============================

.. code:: foam

    volScalarField p_static(IOobject( "p_static", runTime.timeName(), mesh, IOobject::NO_READ, IOobject::AUTO_WRITE), p);

    fvScalarMatrix psEqn( fvm::div(phi_g,p_static) == rho*magSqr(g) );
    psEqn.solve();
    volScalarField p_dynamic("p_dynamic", p - p_static);
    p_static.write();
    p_dynamic.write();

The complete implementation can be found at |psFoam|.

How to use
==================

The |psFoam| shares all the libraries available to |darcyfoam|, and it is usually used in the case of |darcyFoam|.
In order to calculate hydrostatic pressure of a result of |darcyFoam|, just need to add discretization scheme and solution controls in the system/fvSchemes and system/fvSolution files, respectively. See see :numref:`lst:system:fvSchemes:psFoam`. 
Then run the solver command `HydrostaticPressureFoam`, you will get :code:`p_static` and :code:`p_dynamic` field in the latest time folder.

.. tab:: fvSchemes

    .. code-block:: foam
        :linenos:
        :name: lst:system:fvSchemes:psFoam
        :caption: fvSolution configuration of |psFoam|.
        :emphasize-lines: 6, 13

        gradSchemes
        {
            default         none;
            grad(p)         Gauss linear;
            grad(T)         Gauss linear;
            grad(p_static)       Gauss linear;
        }
        divSchemes
        {
            default         none;
            div(phi,T)      Gauss upwind;
            div((phi*interpolate(Cp)),T) Gauss vanLeer;
            div(phi_g,p_static)  Gauss upwind;
        }

.. tab:: fvSolution

    .. code-block:: 
        :linenos:
        :name: lst:system:fvSolution:psFoam
        :caption: fvSchemes configuration of |psFoam|.
        :emphasize-lines: 15-26
        
        solvers
        {
            p
            {
                solver          PCG;
                preconditioner  DIC;
                tolerance       1e-12;
                relTol          0;
            }
            pFinal
            {
                $p;
                relTol          0;
            }
            p_static
            {
                solver          PBiCG;
                preconditioner  DILU;
                tolerance       1e-06;
                relTol          0;
            }
            p_staticFinal
            {
                $p_static;
                relTol          0;
            }
            T
            {
                solver          PBiCG;
                preconditioner  DILU;
                tolerance       1e-06;
                relTol          0;
            }
            TFinal
            {
                $T;
                relTol          0;
            }
        }

Result of :ref:`sec_cookbook_pipe_2D` case
=====================================================================================

.. tab:: Hydrostatic pressure 

    .. figure:: /_figures/p_s_clip.jpeg
        :align: center
        :name: fig:pipe:p_s

        Hydrostatic pressure solved by |psFoam|

.. tab:: Dynamic pressure

    .. figure:: /_figures/p_d_clip.jpeg
        :align: center
        :name: fig:pipe:p_d

        Dynamic pressure calculated by subtracting the hydrostatic pressure from the full pressure.

.. tab:: Full pressure

    .. figure:: /_figures/p_clip.jpeg
        :align: center
        :name: fig:pipe:p

        Full pressure solved by |darcyFoam|

.. tab:: Forward test

    .. figure:: /_figures/p_s_forward_check_clip.jpeg
        :align: center
        :name: fig:pipe:p_s_check

        Test the hydrostatic pressure calculation: :code:`fvc::grad(p_static) - rho*g`


.. admonition:: Extension (举一反三)

      We can solve any gradient equation just like the above steps, and could also get the matrix of implicit operator :code:`fvm::grad()` even though OpenFOAM doesn't provide it.