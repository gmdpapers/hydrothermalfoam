.. _installation:


*******************
Installation
*******************

.. include:: /include.rst_

.. tip::

   We provide a 5-minutes quick start video (on Mac OS) that can be accessed here `https://youtu.be/6czcxC90gp0 <https://youtu.be/6czcxC90gp0>`_.
   

There are two ways to install |foam|. 
The easiest way is intalling via Docker image, see :numref:`sec:install_docker`.
Another way is building from source code if user has experience of OpenFOAM installation, see section of :numref:`installation_source`.

.. toctree::
   :maxdepth: 2

   docker.rst
   source.rst