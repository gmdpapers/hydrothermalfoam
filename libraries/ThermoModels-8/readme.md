# Difference between v7 and v8

## 1. IAPWSThermoI.H, IAPWSThermo.H

## 2. htHydroThermo.* 

The multiphase properties are not implemented in v8, bu it is easy to do that just following v7.

## 3. heHydroThermo.C 
This is modified from the official heThermo.C which is pretty different between v7 and v8.

## 4. makeThermo related code
V8 has a new and fancy ways to make thermos, but it is similar to the traditional ways.

`hydroThermos.C` controls all available species, e.g. water, simple, saltwater(need to implement), and there is a `for**.C` in each species folder define the specific thermo models.



# ThermoModels

**File structure**

```
.
├── basic
|   ├── hydroThermo
|   |   ├── hydroThermo
|   |   ├── htHydroThermo
|   ├── heThermo
|   |   ├── heHydroThermo.H
|   |   ├── heHydroThermo.C
|   ├── mixture
|   |   ├── hydrothermalMixture.H
|   |   ├── hydrothermalMixture.C
|   └── energy
|   |   ├── temperatureEnergy.H
|   |   ├── specificEnthalpy.H
├── EOSlib
|   ├── freesteam
├── species
|   ├── water
|   |   ├── waterThermos.C
|   |   ├── transport
|   |   ├── thermo
|   |   ├── equationOfState
├── Make
|   ├── files
|   └── options
└── readme.md
```

## basic
To develop hydrothermal solvers in a much more elegant style. I have to develop some new basic classes.

### hydroThermo
Because OpenFoam native thermo model doesn't have thermal expansivity and compressibility(see eq. 5 in [Hasenclever et al.(2014)](https://doi.org/10.1038/nature13174)). Therefore I have to define a new thermo model including these two properties.
**Note that** `hydroThermo` is based on `rhoThermo`, `htHydroThermo` is based on `heRhoThermo`.

**htHydrothermal.C** implements rho(), mu(), alphaP(), betaT() properties update functions.

### heHydroThermo
Modified from `$FOAM_SRC/thermophysicalModels/basic/heThermo/`, there is a `init()` function in `heHydroThermo.C`, I added a new line to initialize `rho_` which is necessary in `ThermoEquilibrium` function in `IAPWSEOS` class.

### mixture
Because OpenFoam native mixture class, pureMixture, doesn't have right to modify member variables(e.g. SteamState S_, region_ind_) in `htHydrothermo.C`->`calculate()`. But this is important to optimize the properties calculation using `freesteam` or `EOS_H2ONaCl`.
Therefore I have to define a new mixture class to add a new member function of `cellMixtureModifiable`. **Note that** `hydrothermalMixture` is based on `pureMixture`

HydrothermalSinglePhaseDarcyFoam calls `hydroThermo` in createField.H.

### energy
OpenFoam have two energy type, enthalpy (h) and interal energy (e), but doesn't have temperature (T) type. To formulate energy conservation equation in terms temperature in hydrothermal modeling, I have to define a new energy class to mark this.
**Note that** `temperatureEnergy` is based on `sensibleEnthalpy`.

## species
This folder contains thermophysical models.
### water
The properties of water is implemented using freesteam (IAPWS-IF97).

### seawater
The properties of seawater is implemented using Diresner(2007) equations.

## EOSlib
This folder contains head files EOS library

### freesteam

### EOS_H2ONaCl