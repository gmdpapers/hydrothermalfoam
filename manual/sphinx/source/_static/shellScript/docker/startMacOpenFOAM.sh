#----------------------------------*-sh-*--------------------------------------
# =========                 |
# \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
#  \\    /   O peration     |
#   \\  /    A nd           | 
#    \\/     M anipulation  | Copyright (C) 2016-2019 OpenCFD Ltd.
#------------------------------------------------------------------------------
# License
#     This file is part of HydrothermalFoam.
#
# File
#     startMacHydrothermalFoam
#
# Description
#     1) Start HydrothermalFoam container with name 'hydrothermalfoam'
#     2) To login as root
#          * Root: to login as root run command "su root"
#           password: hydrothermalfoam
#
# NOTE:
#     This script should be placed in the home area 
#
#     To post-process, please install Paraview for your MAC seprately
#
#------------------------------------------------------------------------------
# you have to configure XQuartzs ifrstly: (1) XQuartzs -> Preferences -> Security -> Authenticate connections
#                                         (2) XQuartzs -> Preferences -> Security -> Allow connections from network clients
# then exit XQuartzs and open it again
IP="127.0.0.1"
export DISPLAY=$IP":0.0"
xhost + $IP
docker start  hydrothermalfoam
docker attach hydrothermalfoam
