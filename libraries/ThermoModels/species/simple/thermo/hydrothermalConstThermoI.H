/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2011-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

template<class EquationOfState>
inline Foam::hydrothermalConstThermo<EquationOfState>::hydrothermalConstThermo
(
    const EquationOfState& st,
    const scalar cp
)
:
    EquationOfState(st),
    Cp_(cp)
{}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template<class EquationOfState>
inline Foam::hydrothermalConstThermo<EquationOfState>::hydrothermalConstThermo
(
    const word& name,
    const hydrothermalConstThermo& ct
)
:
    EquationOfState(name, ct),
    Cp_(ct.Cp_)
{}


template<class EquationOfState>
inline Foam::autoPtr<Foam::hydrothermalConstThermo<EquationOfState>>
Foam::hydrothermalConstThermo<EquationOfState>::clone() const
{
    return autoPtr<hydrothermalConstThermo<EquationOfState>>
    (
        new hydrothermalConstThermo<EquationOfState>(*this)
    );
}


template<class EquationOfState>
inline Foam::autoPtr<Foam::hydrothermalConstThermo<EquationOfState>>
Foam::hydrothermalConstThermo<EquationOfState>::New(const dictionary& dict)
{
    return autoPtr<hydrothermalConstThermo<EquationOfState>>
    (
        new hydrothermalConstThermo<EquationOfState>(dict)
    );
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template<class EquationOfState>
inline Foam::scalar Foam::hydrothermalConstThermo<EquationOfState>::limit
(
    const scalar T
) const
{
    return T;
}


template<class EquationOfState>
inline Foam::scalar Foam::hydrothermalConstThermo<EquationOfState>::Cp
(
    const scalar p,
    const scalar T
) const
{
    return Cp_ + EquationOfState::Cp(p, T);
}


template<class EquationOfState>
inline Foam::scalar Foam::hydrothermalConstThermo<EquationOfState>::Hs
(
    const scalar p, const scalar T
) const
{
    // Info<<"测试simple的Hs函数"<<endl;
    // return Cp_*T + EquationOfState::H(p, T);
    // Return specific enthalpy
    return EquationOfState::H(p, T);
}


template<class EquationOfState>
inline Foam::scalar Foam::hydrothermalConstThermo<EquationOfState>::Hc() const
{
    return 0;
}


template<class EquationOfState>
inline Foam::scalar Foam::hydrothermalConstThermo<EquationOfState>::Ha
(
    const scalar p, const scalar T
) const
{
    return Hs(p, T) + Hc();
}


template<class EquationOfState>
inline Foam::scalar Foam::hydrothermalConstThermo<EquationOfState>::S
(
    const scalar p, const scalar T
) const
{
    return Cp_*log(T/Tstd) + EquationOfState::S(p, T);
}


template<class EquationOfState>
inline Foam::scalar Foam::hydrothermalConstThermo<EquationOfState>::dGdT
(
    const scalar p, const scalar T
) const
{
    return 0;
}


template<class EquationOfState>
inline Foam::scalar Foam::hydrothermalConstThermo<EquationOfState>::dCpdT
(
    const scalar p, const scalar T
) const
{
    return 0;
}


// * * * * * * * * * * * * * * * Member Operators  * * * * * * * * * * * * * //

template<class EquationOfState>
inline void Foam::hydrothermalConstThermo<EquationOfState>::operator+=
(
    const hydrothermalConstThermo<EquationOfState>& ct
)
{
    scalar Y1 = this->Y();

    EquationOfState::operator+=(ct);

    if (mag(this->Y()) > small)
    {
        Y1 /= this->Y();
        scalar Y2 = ct.Y()/this->Y();

        Cp_ = Y1*Cp_ + Y2*ct.Cp_;
    }
}


// * * * * * * * * * * * * * * * Friend Operators  * * * * * * * * * * * * * //

template<class EquationOfState>
inline Foam::hydrothermalConstThermo<EquationOfState> Foam::operator+
(
    const hydrothermalConstThermo<EquationOfState>& ct1,
    const hydrothermalConstThermo<EquationOfState>& ct2
)
{
    EquationOfState eofs
    (
        static_cast<const EquationOfState&>(ct1)
      + static_cast<const EquationOfState&>(ct2)
    );

    if (mag(eofs.Y()) < small)
    {
        return hydrothermalConstThermo<EquationOfState>
        (
            eofs,
            ct1.Cp_
        );
    }
    else
    {
        return hydrothermalConstThermo<EquationOfState>
        (
            eofs,
            ct1.Y()/eofs.Y()*ct1.Cp_
          + ct2.Y()/eofs.Y()*ct2.Cp_
        );
    }
}


template<class EquationOfState>
inline Foam::hydrothermalConstThermo<EquationOfState> Foam::operator*
(
    const scalar s,
    const hydrothermalConstThermo<EquationOfState>& ct
)
{
    return hydrothermalConstThermo<EquationOfState>
    (
        s*static_cast<const EquationOfState&>(ct),
        ct.Cp_
    );
}


template<class EquationOfState>
inline Foam::hydrothermalConstThermo<EquationOfState> Foam::operator==
(
    const hydrothermalConstThermo<EquationOfState>& ct1,
    const hydrothermalConstThermo<EquationOfState>& ct2
)
{
    EquationOfState eofs
    (
        static_cast<const EquationOfState&>(ct1)
     == static_cast<const EquationOfState&>(ct2)
    );

    return hydrothermalConstThermo<EquationOfState>
    (
        eofs,
        ct2.Y()/eofs.Y()*ct2.Cp_
      - ct1.Y()/eofs.Y()*ct1.Cp_
    );
}


// ************************************************************************* //
