.. define short name variables

.. _OpenFOAM: https://openfoam.org

.. _Gmsh: http://gmsh.info

.. _Docker: https://www.docker.com/products/docker-desktop

.. _freesteam: http://freesteam.sourceforge.net

.. _ParaView: https://www.paraview.org

.. _Tecplot: https://www.tecplot.com

.. _tmux: https://github.com/tmux/tmux/wiki

.. _HydrothermalFoam: https://gitlab.com/gmdpapers/hydrothermalfoam

.. _HydrostaticPressureFoam: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/solvers/HydrothermalSinglePhaseDarcyFoam/HydrostaticPressureFoam

.. _HydrothermalSinglePhaseDarcyFoam: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/solvers/HydrothermalSinglePhaseDarcyFoam

.. _scons: https://scons.org

.. _cmake: https://cmake.org

.. _python 2: https://www.python.org/downloads/release/python-272/

.. _GSL: https://www.gnu.org/software/gsl/doc/html/

.. _freesteam-2.1: http://freesteam.sourceforge.net

.. _IAPWS-IF97: http://www.iapws.org/relguide/IF97-Rev.html

.. _cookbooks: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks

.. _benchmarks: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/benchmarks

.. _cookbooks/helloworld: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/helloworld

.. _cookbooks/nonUniformFixedValueBC: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/nonUniformFixedValueBC

.. _cookbooks/timeDependentPerm: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/timeDependentPerm

.. _cookbooks/gmsh: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/gmsh

.. _cookbooks/3Dbox: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/3Dbox

.. _cookbooks/3Dbox_par: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/3Dbox_par

.. _cookbooks/pipe: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/pipe

.. _cookbooks/pipe_3D: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/pipe_3D

.. _cookbooks/singlepass: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/singlepass

.. _cookbooks/singlepass_twolimb: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/singlepass_twolimb

.. _cookbooks/singlepass_3D: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/master/cookbooks/singlepass_3D

.. _502c669e: https://gitlab.com/gmdpapers/hydrothermalfoam/-/tree/502c669ed7d37429490eea1ed8fd0f02d5bfba12/libraries

.. _0c42a6c2: https://gitlab.com/gmdpapers/hydrothermalfoam/-/commit/0c42a6c2cd48fdff147e3b1c045c5ac4cf67fee8

.. _a0419165: https://gitlab.com/gmdpapers/hydrothermalfoam/-/commit/a0419165b0fb520b97c25b6fa487592447981552

.. _60232254: https://gitlab.com/gmdpapers/hydrothermalfoam/-/commit/60232254645a49c07ccbbd6fe6ed327d7b8d4031

.. _f8572e49: https://gitlab.com/gmdpapers/hydrothermalfoam/-/commit/f8572e4922140221e77526f09cdce94d90fe129f

.. _50967d54: https://gitlab.com/gmdpapers/hydrothermalfoam/-/commit/50967d54a0cb225588547c69fb688e43721eef7e

.. |helloworld| replace:: `cookbooks/helloworld`_

.. |nonUniformFixedValueBC| replace:: `cookbooks/nonUniformFixedValueBC`_

.. |timeDependentPerm| replace:: `cookbooks/timeDependentPerm`_

.. |gmsh| replace:: `cookbooks/gmsh`_

.. |3Dbox| replace:: `cookbooks/3Dbox`_

.. |3Dbox_par| replace:: `cookbooks/3Dbox_par`_

.. |pipe| replace:: `cookbooks/pipe`_

.. |pipe_3D| replace:: `cookbooks/pipe_3D`_

.. |singlepass| replace:: `cookbooks/singlepass`_

.. |singlepass_twolimb| replace:: `cookbooks/singlepass_twolimb`_

.. |singlepass_3D| replace:: `cookbooks/singlepass_3D`_


.. |foam| replace:: HydrothermalFoam_

.. |darcyfoam| replace:: HydrothermalSinglePhaseDarcyFoam_

.. |psFoam| replace:: HydrostaticPressureFoam_

.. |anhydritefoam| replace:: HydrothermalSinglePhaseDarcyAnhydriteFoam

.. |paper_hydrothermalfoam| replace:: the manuscript(Guo, Ruepke & Tao, 2020)

.. |degC| replace:: :math:`^{\circ}\text{C}`


