
## Parallel computing performance and comparison with serial computing

### [`fixedValue` temperature boundary condition and `fixedFluxPressure` pressure boundary condition](./fixedTfixedFluxPressure)

![](https://scibyte.oss-cn-beijing.aliyuncs.com/HydrothermalFoam/Serial_Parallel/T_Parallel%20computing_2DBox_fixedFluxPressure.mp4)

### [`fixedValue` temperature boundary condition and `noFlux`  pressure boundary condition](./fixedTnoFlux)

### [`fixedHeatFlux` for temperature boundary condition](./fixedHeatFlux)

### [`fixedGradient` for temperature boundary condition](./fixedGradient_T)